-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 18, 2018 at 12:25 PM
-- Server version: 5.7.20
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `planters-h5`
--

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL COMMENT 'ID',
  `openid` varchar(255) NOT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `browser` varchar(255) DEFAULT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='客户表';

-- --------------------------------------------------------

--
-- Table structure for table `yinpian`
--

CREATE TABLE `yinpian` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `img_mm` varchar(255) NOT NULL,
  `img_bb` varchar(255) NOT NULL,
  `img_ne` varchar(255) NOT NULL,
  `style` varchar(255) NOT NULL,
  `create_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `yinpian`
--

INSERT INTO `yinpian` (`id`, `user_id`, `img_mm`, `img_bb`, `img_ne`, `style`, `create_time`) VALUES
(1, 0, '/uploads/5a5d78addbf21_mm.png', '/uploads/5a5d78addbf21_bb.png', '/uploads/5a5d78addbf21_ne.png', 'yinpian1', '2018-01-16 11:59:41'),
(2, 0, '/uploads/5a5d78dd5c240_mm.png', '/uploads/5a5d78dd5c240_bb.png', '/uploads/5a5d78dd5c240_ne.png', 'yinpian1', '2018-01-16 12:00:29'),
(3, 0, '/uploads/5a5d793dcbddb_mm.png', '/uploads/5a5d793dcbddb_bb.png', '/uploads/5a5d793dcbddb_ne.png', 'yinpian1', '2018-01-16 12:02:05'),
(4, 0, '', '', '/uploads/5a5dcd256acf2_ne.png', 'yinpian2', '2018-01-16 18:00:05'),
(5, 0, '', '', '', 'yinpian3', '2018-01-17 13:52:55'),
(6, 0, '', '', '', 'yinpian3', '2018-01-17 13:53:40'),
(7, 0, '', '', '', 'yinpian3', '2018-01-17 14:29:58'),
(8, 0, '/uploads/5a5eed75641d0_mm.png', '', '', 'yinpian3', '2018-01-17 14:30:13'),
(9, 0, '', '', '', 'yinpian3', '2018-01-17 14:41:04'),
(10, 0, '', '', '', 'yinpian3', '2018-01-17 14:41:46'),
(11, 0, '/uploads/5a5efd98af58d_mm.png', '', '', 'yinpian3', '2018-01-17 15:39:04'),
(12, 0, '/uploads/5a5efe8e98e2f_mm.png', '', '', 'yinpian3', '2018-01-17 15:43:10'),
(13, 0, '/uploads/5a5f03b8ca51a_mm.png', '', '', 'yinpian3', '2018-01-17 16:05:12'),
(14, 0, '/uploads/5a5f03f30d977_mm.png', '', '', 'yinpian3', '2018-01-17 16:06:11'),
(15, 0, '', '', '', 'yinpian3', '2018-01-17 16:06:24'),
(16, 0, '', '', '', 'yinpian3', '2018-01-17 16:06:41'),
(17, 0, '', '', '', 'yinpian3', '2018-01-17 16:08:17'),
(18, 0, '/uploads/5a5f058dc9db5_mm.png', '', '', 'yinpian3', '2018-01-17 16:13:01'),
(19, 0, '/uploads/5a5f06498a9b1_mm.png', '', '', 'yinpian3', '2018-01-17 16:16:09'),
(20, 0, '/uploads/5a5f06acc075b_mm.png', '', '', 'yinpian3', '2018-01-17 16:17:48'),
(21, 0, '', '', '/uploads/5a5f073a8f2c3_ne.png', 'yinpian3', '2018-01-17 16:20:10'),
(22, 0, '/uploads/5a5f07df52c33_mm.png', '', '', 'yinpian3', '2018-01-17 16:22:55'),
(23, 0, '/uploads/5a5f07f52c7c5_mm.png', '', '', 'yinpian3', '2018-01-17 16:23:17'),
(24, 0, '/uploads/5a5f087daef35_mm.png', '', '', 'yinpian3', '2018-01-17 16:25:33'),
(25, 0, '/uploads/5a5f0a39e0975_mm.png', '', '', 'yinpian3', '2018-01-17 16:32:57'),
(26, 0, '/uploads/5a5f0a40ebac8_mm.png', '', '', 'yinpian3', '2018-01-17 16:33:04'),
(27, 0, '/uploads/5a5f0a4caa769_mm.png', '', '', 'yinpian3', '2018-01-17 16:33:16'),
(28, 0, '/uploads/5a5f0b24eb6c9_mm.png', '', '', 'yinpian3', '2018-01-17 16:36:52'),
(29, 0, '', '', '/uploads/5a5f0bcc88110_ne.png', 'yinpian3', '2018-01-17 16:39:40'),
(30, 0, '', '/uploads/5a5f0c96b2ee5_bb.png', '', 'yinpian3', '2018-01-17 16:43:02'),
(31, 0, '/uploads/5a5f0d0dd2037_mm.png', '', '', 'yinpian3', '2018-01-17 16:45:01'),
(32, 0, '', '', '', 'yinpian3', '2018-01-17 16:50:19'),
(33, 0, '/uploads/5a5f0fa7a714b_mm.png', '', '', 'yinpian3', '2018-01-17 16:56:07'),
(34, 0, '', '', '', 'yinpian3', '2018-01-17 16:57:29'),
(35, 0, '', '', '', 'yinpian3', '2018-01-17 16:58:10'),
(36, 0, '', '', '', 'yinpian3', '2018-01-17 16:59:59'),
(37, 0, '', '', '', 'yinpian3', '2018-01-17 17:02:40'),
(38, 0, '', '', '', 'yinpian3', '2018-01-17 17:03:45'),
(39, 0, '', '', '', 'yinpian3', '2018-01-17 17:05:26'),
(40, 0, '', '', '', 'yinpian3', '2018-01-17 17:06:39'),
(41, 0, '/uploads/5a5f1606cb737_mm.png', '', '', 'yinpian3', '2018-01-17 17:23:18'),
(42, 0, '/uploads/5a5f1663e07f5_mm.png', '', '', 'yinpian3', '2018-01-17 17:24:51'),
(43, 0, '/uploads/5a5f1752ac66e_mm.png', '', '', 'yinpian3', '2018-01-17 17:28:50'),
(44, 0, '/uploads/5a5f178056771_mm.png', '', '', 'yinpian3', '2018-01-17 17:29:36'),
(45, 0, '/uploads/5a5f1ad83130a_mm.png', '', '', 'yinpian3', '2018-01-17 17:43:52'),
(46, 0, '/uploads/5a5f1bbf10fc8_mm.png', '', '', 'yinpian3', '2018-01-17 17:47:43'),
(47, 0, '/uploads/5a5f1bfcbe5f5_mm.png', '', '', 'yinpian3', '2018-01-17 17:48:44'),
(48, 0, '/uploads/5a5f1c4e5b939_mm.png', '', '', 'yinpian3', '2018-01-17 17:50:06'),
(49, 0, '/uploads/5a5f1c9b3a549_mm.png', '', '', 'yinpian3', '2018-01-17 17:51:23'),
(50, 0, '/uploads/5a5f1cb4ec80b_mm.png', '', '', 'yinpian3', '2018-01-17 17:51:48'),
(51, 0, '/uploads/5a5f1d241ccd8_mm.png', '', '', 'yinpian3', '2018-01-17 17:53:40'),
(52, 0, '/uploads/5a5f1dfa0e427_mm.png', '', '', 'yinpian3', '2018-01-17 17:57:14'),
(53, 0, '/uploads/5a5f1e67f2153_mm.png', '', '', 'yinpian3', '2018-01-17 17:59:03'),
(54, 0, '/uploads/5a5f1ef05cb83_mm.png', '', '', 'yinpian3', '2018-01-17 18:01:20'),
(55, 0, '/uploads/5a5f1f71353ac_mm.png', '', '', 'yinpian3', '2018-01-17 18:03:29'),
(56, 0, '/uploads/5a5f1f862b8c1_mm.png', '/uploads/5a5f1f862b8c1_bb.png', '/uploads/5a5f1f862b8c1_ne.png', 'yinpian3', '2018-01-17 18:03:50'),
(57, 0, '', '/uploads/5a5f20140bb74_bb.png', '', 'yinpian1', '2018-01-17 18:06:12'),
(58, 0, '', '', '', 'yinpian1', '2018-01-17 19:01:27'),
(59, 0, '', '', '', 'yinpian1', '2018-01-17 19:04:40'),
(60, 0, '', '/uploads/5a5f2e0dab3e3_bb.png', '', 'yinpian1', '2018-01-17 19:05:49'),
(61, 0, '', '/uploads/5a5f2e496f301_bb.png', '', 'yinpian1', '2018-01-17 19:06:49'),
(62, 0, '', '', '/uploads/5a5f2e5926112_ne.png', 'yinpian1', '2018-01-17 19:07:05'),
(63, 0, '', '', '/uploads/5a5f2e82b44ad_ne.png', 'yinpian1', '2018-01-17 19:07:46'),
(64, 0, '', '', '/uploads/5a5f2eac82274_ne.png', 'yinpian1', '2018-01-17 19:08:28'),
(65, 0, '', '', '/uploads/5a5f2eef7a4c2_ne.png', 'yinpian1', '2018-01-17 19:09:35'),
(66, 0, '', '', '/uploads/5a5f2f27f05cb_ne.png', 'yinpian1', '2018-01-17 19:10:31'),
(67, 0, '/uploads/5a5f2f70d3381_mm.png', '', '', 'yinpian1', '2018-01-17 19:11:44'),
(68, 0, '/uploads/5a5f2f9b06b7d_mm.png', '', '', 'yinpian1', '2018-01-17 19:12:27'),
(69, 0, '/uploads/5a5f2fe31dfc6_mm.png', '/uploads/5a5f2fe31dfc6_bb.png', '/uploads/5a5f2fe31dfc6_ne.png', 'yinpian1', '2018-01-17 19:13:39'),
(70, 0, '/uploads/5a5f2fef75956_mm.png', '/uploads/5a5f2fef75956_bb.png', '/uploads/5a5f2fef75956_ne.png', 'yinpian1', '2018-01-17 19:13:51'),
(71, 0, '/uploads/5a5f2ffda189e_mm.png', '/uploads/5a5f2ffda189e_bb.png', '/uploads/5a5f2ffda189e_ne.png', 'yinpian1', '2018-01-17 19:14:05'),
(72, 0, '/uploads/5a5f3301462eb_mm.png', '', '', 'yinpian2', '2018-01-17 19:26:57'),
(73, 0, '', '', '', 'yinpian2', '2018-01-17 19:28:04'),
(74, 0, '/uploads/5a5f34b36c60f_mm.png', '', '', 'yinpian2', '2018-01-17 19:34:11'),
(75, 0, '', '', '', 'yinpian4', '2018-01-17 19:39:40'),
(76, 0, '/uploads/5a5f36ad47abf_mm.png', '/uploads/5a5f36ad47abf_bb.png', '/uploads/5a5f36ad47abf_ne.png', 'yinpian4', '2018-01-17 19:42:37'),
(77, 0, '/uploads/5a5f36d0580a3_mm.png', '', '', 'yinpian4', '2018-01-17 19:43:12'),
(78, 0, '/uploads/5a5f371d366be_mm.png', '', '', 'yinpian4', '2018-01-17 19:44:29'),
(79, 0, '', '', '/uploads/5a5f372fb26e3_ne.png', 'yinpian4', '2018-01-17 19:44:47'),
(80, 0, '', '', '/uploads/5a5f374fa02ed_ne.png', 'yinpian4', '2018-01-17 19:45:19'),
(81, 0, '', '', '/uploads/5a5f37cab3830_ne.png', 'yinpian4', '2018-01-17 19:47:22'),
(82, 0, '', '', '/uploads/5a5f37ea0551e_ne.png', 'yinpian4', '2018-01-17 19:47:54'),
(83, 0, '', '/uploads/5a5f3959986fc_bb.png', '', 'yinpian4', '2018-01-17 19:54:01'),
(84, 0, '', '/uploads/5a5f397199360_bb.png', '', 'yinpian4', '2018-01-17 19:54:25'),
(85, 0, '', '/uploads/5a5f39b68aff9_bb.png', '', 'yinpian4', '2018-01-17 19:55:34'),
(86, 0, '', '/uploads/5a5f39ef4cc0d_bb.png', '', 'yinpian4', '2018-01-17 19:56:31'),
(87, 0, '', '/uploads/5a5f3a3288b3a_bb.png', '', 'yinpian4', '2018-01-17 19:57:38'),
(88, 0, '', '', '/uploads/5a5f3d6b4373c_ne.png', 'yinpian1', '2018-01-17 20:11:23'),
(89, 0, '/uploads/5a5f3e338066f_mm.png', '/uploads/5a5f3e338066f_bb.png', '/uploads/5a5f3e338066f_ne.png', 'yinpian1', '2018-01-17 20:14:43'),
(90, 0, '', '', '', 'yinpian1', '2018-01-17 20:15:06'),
(91, 0, '', '', '', 'yinpian1', '2018-01-17 20:15:21'),
(92, 0, '', '', '', 'yinpian1', '2018-01-17 20:16:06'),
(93, 0, '', '', '', 'yinpian1', '2018-01-17 20:16:33'),
(94, 0, '', '', '', 'yinpian1', '2018-01-17 20:18:52'),
(95, 0, '', '', '', 'yinpian1', '2018-01-17 20:19:07'),
(96, 0, '', '', '', 'yinpian1', '2018-01-17 20:19:25'),
(97, 0, '', '', '', 'yinpian1', '2018-01-17 20:58:01'),
(98, 0, '/uploads/5a5f4882a8512_mm.png', '/uploads/5a5f4882a8512_bb.png', '/uploads/5a5f4882a8512_ne.png', 'yinpian1', '2018-01-17 20:58:42'),
(99, 0, '/uploads/5a5f48f46902b_mm.png', '/uploads/5a5f48f46902b_bb.png', '/uploads/5a5f48f46902b_ne.png', 'yinpian3', '2018-01-17 21:00:36'),
(100, 0, '/uploads/5a5f499bce6c2_mm.png', '/uploads/5a5f499bce6c2_bb.png', '/uploads/5a5f499bce6c2_ne.png', 'yinpian3', '2018-01-17 21:03:24'),
(101, 0, '/uploads/5a5f49c7284e9_mm.png', '/uploads/5a5f49c7284e9_bb.png', '/uploads/5a5f49c7284e9_ne.png', 'yinpian3', '2018-01-17 21:04:07'),
(102, 0, '/uploads/5a5f49e506965_mm.png', '/uploads/5a5f49e506965_bb.png', '/uploads/5a5f49e506965_ne.png', 'yinpian3', '2018-01-17 21:04:37'),
(103, 0, '', '', '', 'yinpian1', '2018-01-18 11:42:39'),
(104, 0, '', '', '/uploads/5a60190a270e1_ne.png', 'yinpian1', '2018-01-18 11:48:26'),
(105, 0, '', '', '/planters-h5/uploads/5a601931ec06a_ne.png', 'yinpian1', '2018-01-18 11:49:05'),
(106, 0, '', '', '/planters-h5/uploads/5a60195a34df1_ne.png', 'yinpian1', '2018-01-18 11:49:46'),
(107, 0, '', '', '/planters-h5/uploads/5a6019e30820d_ne.png', 'yinpian1', '2018-01-18 11:52:03'),
(108, 0, '/planters-h5/uploads/5a601a15777bc_mm.png', '/planters-h5/uploads/5a601a15777bc_bb.png', '/planters-h5/uploads/5a601a15777bc_ne.png', 'yinpian1', '2018-01-18 11:52:53'),
(109, 0, '/planters-h5/uploads/5a601accdc905_mm.png', '/planters-h5/uploads/5a601accdc905_bb.png', '/planters-h5/uploads/5a601accdc905_ne.png', 'yinpian1', '2018-01-18 11:55:56'),
(110, 0, '/planters-h5/uploads/5a601df851811_mm.png', '/planters-h5/uploads/5a601df851811_bb.png', '/planters-h5/uploads/5a601df851811_ne.png', 'yinpian1', '2018-01-18 12:09:28'),
(111, 0, '/planters-h5/uploads/5a60289b7a7af_mm.png', '/planters-h5/uploads/5a60289b7a7af_bb.png', '/planters-h5/uploads/5a60289b7a7af_ne.png', 'yinpian1', '2018-01-18 12:54:51'),
(112, 0, '', '/planters-h5/uploads/5a6028e4b03a8_bb.png', '', 'yinpian1', '2018-01-18 12:56:04'),
(113, 0, '', '/planters-h5/uploads/5a6029824e48c_bb.png', '', 'yinpian1', '2018-01-18 12:58:42'),
(114, 0, '/planters-h5/uploads/5a602a1d84e76_mm.png', '/planters-h5/uploads/5a602a1d84e76_bb.png', '/planters-h5/uploads/5a602a1d84e76_ne.png', 'yinpian1', '2018-01-18 13:01:17'),
(115, 0, '/planters-h5/uploads/5a602a71253e3_mm.png', '/planters-h5/uploads/5a602a71253e3_bb.png', '/planters-h5/uploads/5a602a71253e3_ne.png', 'yinpian1', '2018-01-18 13:02:41'),
(116, 0, '/planters-h5/uploads/5a602a881cb7e_mm.png', '', '', 'yinpian1', '2018-01-18 13:03:04'),
(117, 0, '/planters-h5/uploads/5a602d5948c37_mm.png', '/planters-h5/uploads/5a602d5948c37_bb.png', '/planters-h5/uploads/5a602d5948c37_ne.png', 'yinpian1', '2018-01-18 13:15:05'),
(118, 0, '/planters-h5/uploads/5a602d80a1c89_mm.png', '', '', 'yinpian1', '2018-01-18 13:15:44'),
(119, 0, '', '', '/planters-h5/uploads/5a602d99e3f3f_ne.png', 'yinpian1', '2018-01-18 13:16:09'),
(120, 0, '', '', '/planters-h5/uploads/5a602db08eff6_ne.png', 'yinpian1', '2018-01-18 13:16:32'),
(121, 0, '', '', '/planters-h5/uploads/5a602dd13b41b_ne.png', 'yinpian1', '2018-01-18 13:17:05'),
(122, 0, '/planters-h5/uploads/5a602e371aabb_mm.png', '', '', 'yinpian1', '2018-01-18 13:18:47'),
(123, 0, '/planters-h5/uploads/5a602e88434b1_mm.png', '', '', 'yinpian1', '2018-01-18 13:20:08'),
(124, 0, '/planters-h5/uploads/5a602ea1d598f_mm.png', '', '', 'yinpian1', '2018-01-18 13:20:33'),
(125, 0, '/planters-h5/uploads/5a602f14dde5f_mm.png', '', '', 'yinpian1', '2018-01-18 13:22:28'),
(126, 0, '', '', '', 'yinpian3', '2018-01-18 14:52:22'),
(127, 0, '', '/planters-h5/uploads/5a604611dcd80_bb.png', '', 'yinpian3', '2018-01-18 15:00:33'),
(128, 0, '/planters-h5/uploads/5a604644a472a_mm.png', '/planters-h5/uploads/5a604644a472a_bb.png', '/planters-h5/uploads/5a604644a472a_ne.png', 'yinpian3', '2018-01-18 15:01:24'),
(129, 0, '/planters-h5/uploads/5a6046a3985ba_mm.png', '/planters-h5/uploads/5a6046a3985ba_bb.png', '/planters-h5/uploads/5a6046a3985ba_ne.png', 'yinpian3', '2018-01-18 15:02:59'),
(130, 0, '/planters-h5/uploads/5a6047ba180c6_mm.png', '/planters-h5/uploads/5a6047ba180c6_bb.png', '/planters-h5/uploads/5a6047ba180c6_ne.png', 'yinpian3', '2018-01-18 15:07:38'),
(131, 0, '/planters-h5/uploads/5a6047f32660a_mm.png', '/planters-h5/uploads/5a6047f32660a_bb.png', '/planters-h5/uploads/5a6047f32660a_ne.png', 'yinpian3', '2018-01-18 15:08:35'),
(132, 0, '/planters-h5/uploads/5a60481e378f2_mm.png', '/planters-h5/uploads/5a60481e378f2_bb.png', '/planters-h5/uploads/5a60481e378f2_ne.png', 'yinpian3', '2018-01-18 15:09:18'),
(133, 0, '/planters-h5/uploads/5a604e7b015d5_mm.png', '/planters-h5/uploads/5a604e7b015d5_bb.png', '/planters-h5/uploads/5a604e7b015d5_ne.png', 'yinpian3', '2018-01-18 15:36:27'),
(134, 0, '/planters-h5/uploads/5a604f4799381_mm.png', '/planters-h5/uploads/5a604f4799381_bb.png', '/planters-h5/uploads/5a604f4799381_ne.png', 'yinpian3', '2018-01-18 15:39:51'),
(135, 0, '/planters-h5/uploads/5a605010ee748_mm.png', '/planters-h5/uploads/5a605010ee748_bb.png', '/planters-h5/uploads/5a605010ee748_ne.png', 'yinpian3', '2018-01-18 15:43:12'),
(136, 0, '', '', '/planters-h5/uploads/5a60504d5ad41_ne.png', 'yinpian3', '2018-01-18 15:44:13'),
(137, 0, '', '', '/planters-h5/uploads/5a605094027c7_ne.png', 'yinpian3', '2018-01-18 15:45:24'),
(138, 0, '', '', '/planters-h5/uploads/5a6050abcfe1c_ne.png', 'yinpian3', '2018-01-18 15:45:47'),
(139, 0, '', '', '/planters-h5/uploads/5a6050cf15e9f_ne.png', 'yinpian3', '2018-01-18 15:46:23'),
(140, 0, '', '', '/planters-h5/uploads/5a60510e21f35_ne.png', 'yinpian3', '2018-01-18 15:47:26'),
(141, 0, '', '', '/planters-h5/uploads/5a6051325d8d8_ne.png', 'yinpian3', '2018-01-18 15:48:02'),
(142, 0, '', '', '/planters-h5/uploads/5a60517e85c35_ne.png', 'yinpian3', '2018-01-18 15:49:18'),
(143, 0, '', '/planters-h5/uploads/5a60519e48fb7_bb.png', '', 'yinpian3', '2018-01-18 15:49:50'),
(144, 0, '', '/planters-h5/uploads/5a6051b755ba0_bb.png', '', 'yinpian3', '2018-01-18 15:50:15'),
(145, 0, '/planters-h5/uploads/5a6051c94ab66_mm.png', '', '', 'yinpian3', '2018-01-18 15:50:33'),
(146, 0, '/planters-h5/uploads/5a6051dd52439_mm.png', '', '', 'yinpian3', '2018-01-18 15:50:53'),
(147, 0, '/planters-h5/uploads/5a6052556caae_mm.png', '', '', 'yinpian3', '2018-01-18 15:52:53'),
(148, 0, '/planters-h5/uploads/5a60528e1962c_mm.png', '', '', 'yinpian3', '2018-01-18 15:53:50'),
(149, 0, '/planters-h5/uploads/5a605390ee3c2_mm.png', '', '', 'yinpian3', '2018-01-18 15:58:08'),
(150, 0, '/planters-h5/uploads/5a6053c57798b_mm.png', '', '', 'yinpian3', '2018-01-18 15:59:01'),
(151, 0, '/planters-h5/uploads/5a6053ef6aa57_mm.png', '', '', 'yinpian3', '2018-01-18 15:59:43'),
(152, 0, '/planters-h5/uploads/5a6054b7ecbf6_mm.png', '', '', 'yinpian3', '2018-01-18 16:03:03'),
(153, 0, '', '', '/planters-h5/uploads/5a6054dd545a4_ne.png', 'yinpian3', '2018-01-18 16:03:41'),
(154, 0, '', '', '/planters-h5/uploads/5a605505e4a25_ne.png', 'yinpian3', '2018-01-18 16:04:21'),
(155, 0, '', '', '/planters-h5/uploads/5a60553c30704_ne.png', 'yinpian3', '2018-01-18 16:05:16'),
(156, 0, '', '/planters-h5/uploads/5a6055e2280b2_bb.png', '/planters-h5/uploads/5a6055e2280b2_ne.png', 'yinpian3', '2018-01-18 16:08:02'),
(157, 0, '', '', '/planters-h5/uploads/5a60579fc4fca_ne.png', 'yinpian3', '2018-01-18 16:15:27'),
(158, 0, '', '', '/planters-h5/uploads/5a6058109803f_ne.png', 'yinpian3', '2018-01-18 16:17:20'),
(159, 0, '', '', '/planters-h5/uploads/5a60584974918_ne.png', 'yinpian3', '2018-01-18 16:18:17'),
(160, 0, '', '', '/planters-h5/uploads/5a6058cabe464_ne.png', 'yinpian3', '2018-01-18 16:20:26'),
(161, 0, '', '', '/planters-h5/uploads/5a6058e67a83a_ne.png', 'yinpian3', '2018-01-18 16:20:54'),
(162, 0, '', '', '/planters-h5/uploads/5a60594b49da5_ne.png', 'yinpian3', '2018-01-18 16:22:35'),
(163, 0, '', '', '/planters-h5/uploads/5a6059815f5a2_ne.png', 'yinpian3', '2018-01-18 16:23:29'),
(164, 0, '', '', '/planters-h5/uploads/5a6059eb6dde5_ne.png', 'yinpian3', '2018-01-18 16:25:15'),
(165, 0, '', '/planters-h5/uploads/5a605a33de2b2_bb.png', '', 'yinpian3', '2018-01-18 16:26:27'),
(166, 0, '', '/planters-h5/uploads/5a605a5541ea4_bb.png', '', 'yinpian3', '2018-01-18 16:27:01'),
(167, 0, '', '/planters-h5/uploads/5a605ac4a2959_bb.png', '', 'yinpian3', '2018-01-18 16:28:52'),
(168, 0, '', '/planters-h5/uploads/5a605af16d89f_bb.png', '', 'yinpian3', '2018-01-18 16:29:37'),
(169, 0, '', '/planters-h5/uploads/5a605b4422e5c_bb.png', '', 'yinpian3', '2018-01-18 16:31:00'),
(170, 0, '', '/planters-h5/uploads/5a605bbc4f00e_bb.png', '', 'yinpian3', '2018-01-18 16:33:00'),
(171, 0, '', '/planters-h5/uploads/5a605c03ba638_bb.png', '', 'yinpian3', '2018-01-18 16:34:11'),
(172, 0, '', '/planters-h5/uploads/5a605cd540b09_bb.png', '', 'yinpian3', '2018-01-18 16:37:41'),
(173, 0, '', '/planters-h5/uploads/5a605d2659bbd_bb.png', '', 'yinpian3', '2018-01-18 16:39:02'),
(174, 0, '', '/planters-h5/uploads/5a605d44e14fd_bb.png', '', 'yinpian3', '2018-01-18 16:39:32'),
(175, 0, '', '/planters-h5/uploads/5a605e17f176e_bb.png', '', 'yinpian3', '2018-01-18 16:43:03'),
(176, 0, '/planters-h5/uploads/5a6060af295d1_mm.png', '', '', 'yinpian1', '2018-01-18 16:54:07'),
(177, 0, '', '/planters-h5/uploads/5a606351ad79d_bb.png', '', 'yinpian1', '2018-01-18 17:05:21'),
(178, 0, '', '/planters-h5/uploads/5a6063fe86af6_bb.png', '', 'yinpian1', '2018-01-18 17:08:14'),
(179, 0, '', '', '/planters-h5/uploads/5a606420a74f9_ne.png', 'yinpian1', '2018-01-18 17:08:48'),
(180, 0, '', '', '/planters-h5/uploads/5a60646492960_ne.png', 'yinpian1', '2018-01-18 17:09:56'),
(181, 0, '/planters-h5/uploads/5a60647b37888_mm.png', '', '', 'yinpian1', '2018-01-18 17:10:19'),
(182, 0, '/planters-h5/uploads/5a6064e055074_mm.png', '', '', 'yinpian1', '2018-01-18 17:12:00'),
(183, 0, '/planters-h5/uploads/5a6064fdec30b_mm.png', '/planters-h5/uploads/5a6064fdec30b_bb.png', '/planters-h5/uploads/5a6064fdec30b_ne.png', 'yinpian1', '2018-01-18 17:12:29'),
(184, 0, '', '/planters-h5/uploads/5a60662453bce_bb.png', '', 'yinpian1', '2018-01-18 17:17:24'),
(185, 0, '', '/planters-h5/uploads/5a606651e7255_bb.png', '', 'yinpian1', '2018-01-18 17:18:09'),
(186, 0, '', '', '/planters-h5/uploads/5a60668578d67_ne.png', 'yinpian1', '2018-01-18 17:19:01'),
(187, 0, '', '', '/planters-h5/uploads/5a60671bc4ef2_ne.png', 'yinpian1', '2018-01-18 17:21:31'),
(188, 0, '', '', '/planters-h5/uploads/5a60672de224b_ne.png', 'yinpian1', '2018-01-18 17:21:49'),
(189, 0, '', '', '/planters-h5/uploads/5a606765b18e8_ne.png', 'yinpian1', '2018-01-18 17:22:45'),
(190, 0, '', '', '/planters-h5/uploads/5a60679e8ac14_ne.png', 'yinpian1', '2018-01-18 17:23:42'),
(191, 0, '', '', '/planters-h5/uploads/5a6067e8c082a_ne.png', 'yinpian1', '2018-01-18 17:24:56'),
(192, 0, '', '', '/planters-h5/uploads/5a606802ec6e0_ne.png', 'yinpian1', '2018-01-18 17:25:22'),
(193, 0, '', '', '/planters-h5/uploads/5a606867678ac_ne.png', 'yinpian1', '2018-01-18 17:27:03'),
(194, 0, '/planters-h5/uploads/5a606894ae2ef_mm.png', '/planters-h5/uploads/5a606894ae2ef_bb.png', '', 'yinpian1', '2018-01-18 17:27:48'),
(195, 0, '/planters-h5/uploads/5a60689faae4d_mm.png', '', '', 'yinpian1', '2018-01-18 17:27:59'),
(196, 0, '/planters-h5/uploads/5a606913ca515_mm.png', '', '', 'yinpian1', '2018-01-18 17:29:55'),
(197, 0, '/planters-h5/uploads/5a606932625e0_mm.png', '', '', 'yinpian1', '2018-01-18 17:30:26'),
(198, 0, '/planters-h5/uploads/5a6069a2aee54_mm.png', '', '', 'yinpian1', '2018-01-18 17:32:18'),
(199, 0, '/planters-h5/uploads/5a6069c06012f_mm.png', '', '', 'yinpian1', '2018-01-18 17:32:48'),
(200, 0, '/planters-h5/uploads/5a6069da8d1c7_mm.png', '', '', 'yinpian1', '2018-01-18 17:33:14'),
(201, 0, '/planters-h5/uploads/5a606a1d336d7_mm.png', '', '', 'yinpian1', '2018-01-18 17:34:21'),
(202, 0, '/planters-h5/uploads/5a606ae132d25_mm.png', '', '', 'yinpian1', '2018-01-18 17:37:37'),
(203, 0, '/planters-h5/uploads/5a606be32bd12_mm.png', '', '', 'yinpian1', '2018-01-18 17:41:55'),
(204, 0, '/planters-h5/uploads/5a606c20b5caf_mm.png', '', '', 'yinpian1', '2018-01-18 17:42:56'),
(205, 0, '/planters-h5/uploads/5a606c8fa07c4_mm.png', '', '', 'yinpian1', '2018-01-18 17:44:47'),
(206, 0, '/planters-h5/uploads/5a606d095df4c_mm.png', '', '', 'yinpian1', '2018-01-18 17:46:49'),
(207, 0, '/planters-h5/uploads/5a606d2181c9e_mm.png', '', '', 'yinpian1', '2018-01-18 17:47:13'),
(208, 0, '/planters-h5/uploads/5a606d4f46223_mm.png', '', '', 'yinpian1', '2018-01-18 17:47:59'),
(209, 0, '/planters-h5/uploads/5a606da2ce8fa_mm.png', '/planters-h5/uploads/5a606da2ce8fa_bb.png', '/planters-h5/uploads/5a606da2ce8fa_ne.png', 'yinpian1', '2018-01-18 17:49:22'),
(210, 0, '/planters-h5/uploads/5a606eb2b8af0_mm.png', '/planters-h5/uploads/5a606eb2b8af0_bb.png', '/planters-h5/uploads/5a606eb2b8af0_ne.png', 'yinpian2', '2018-01-18 17:53:54'),
(211, 0, '/planters-h5/uploads/5a606f25c25cb_mm.png', '/planters-h5/uploads/5a606f25c25cb_bb.png', '/planters-h5/uploads/5a606f25c25cb_ne.png', 'yinpian2', '2018-01-18 17:55:49'),
(212, 0, '/planters-h5/uploads/5a606f68ec00c_mm.png', '', '', 'yinpian2', '2018-01-18 17:56:56'),
(213, 0, '/planters-h5/uploads/5a6070472d429_mm.png', '', '', 'yinpian2', '2018-01-18 18:00:39'),
(214, 0, '/planters-h5/uploads/5a60707e2aa78_mm.png', '', '', 'yinpian2', '2018-01-18 18:01:34'),
(215, 0, '/planters-h5/uploads/5a60708f314b5_mm.png', '', '', 'yinpian2', '2018-01-18 18:01:51'),
(216, 0, '/planters-h5/uploads/5a6070ae4763d_mm.png', '', '', 'yinpian2', '2018-01-18 18:02:22'),
(217, 0, '/planters-h5/uploads/5a6070cd68f47_mm.png', '', '', 'yinpian2', '2018-01-18 18:02:53'),
(218, 0, '/planters-h5/uploads/5a6070f291fce_mm.png', '', '', 'yinpian2', '2018-01-18 18:03:30'),
(219, 0, '/planters-h5/uploads/5a6071015896e_mm.png', '', '', 'yinpian2', '2018-01-18 18:03:45'),
(220, 0, '/planters-h5/uploads/5a60711130067_mm.png', '', '', 'yinpian2', '2018-01-18 18:04:01'),
(221, 0, '/planters-h5/uploads/5a607123b7fdb_mm.png', '', '', 'yinpian2', '2018-01-18 18:04:19'),
(222, 0, '/planters-h5/uploads/5a60713df3405_mm.png', '', '', 'yinpian2', '2018-01-18 18:04:46'),
(223, 0, '', '', '/planters-h5/uploads/5a60715706b0b_ne.png', 'yinpian2', '2018-01-18 18:05:11'),
(224, 0, '', '', '/planters-h5/uploads/5a607167088dc_ne.png', 'yinpian2', '2018-01-18 18:05:27'),
(225, 0, '', '', '/planters-h5/uploads/5a6071931ce99_ne.png', 'yinpian2', '2018-01-18 18:06:11'),
(226, 0, '', '', '/planters-h5/uploads/5a6071c945b8c_ne.png', 'yinpian2', '2018-01-18 18:07:05'),
(227, 0, '', '', '/planters-h5/uploads/5a60727add08e_ne.png', 'yinpian2', '2018-01-18 18:10:02'),
(228, 0, '/planters-h5/uploads/5a607292a2ba9_mm.png', '/planters-h5/uploads/5a607292a2ba9_bb.png', '/planters-h5/uploads/5a607292a2ba9_ne.png', 'yinpian2', '2018-01-18 18:10:26'),
(229, 0, '', '/planters-h5/uploads/5a6072a1c1d14_bb.png', '', 'yinpian2', '2018-01-18 18:10:41'),
(230, 0, '', '/planters-h5/uploads/5a6073356bbc4_bb.png', '', 'yinpian2', '2018-01-18 18:13:09'),
(231, 0, '', '/planters-h5/uploads/5a60734dbede4_bb.png', '', 'yinpian2', '2018-01-18 18:13:33'),
(232, 0, '', '/planters-h5/uploads/5a60736d7c411_bb.png', '', 'yinpian2', '2018-01-18 18:14:05'),
(233, 0, '', '/planters-h5/uploads/5a60738840717_bb.png', '', 'yinpian2', '2018-01-18 18:14:32'),
(234, 0, '/planters-h5/uploads/5a607424581a7_mm.png', '', '', 'yinpian2', '2018-01-18 18:17:08'),
(235, 0, '/planters-h5/uploads/5a60746e3b4cb_mm.png', '/planters-h5/uploads/5a60746e3b4cb_bb.png', '/planters-h5/uploads/5a60746e3b4cb_ne.png', 'yinpian2', '2018-01-18 18:18:22'),
(236, 0, '/planters-h5/uploads/5a60747fbdb74_mm.png', '/planters-h5/uploads/5a60747fbdb74_bb.png', '/planters-h5/uploads/5a60747fbdb74_ne.png', 'yinpian2', '2018-01-18 18:18:39'),
(237, 0, '/planters-h5/uploads/5a60754becdd9_mm.png', '', '', 'yinpian2', '2018-01-18 18:22:03'),
(238, 0, '', '', '/planters-h5/uploads/5a60761638641_ne.png', 'yinpian2', '2018-01-18 18:25:26'),
(239, 0, '/planters-h5/uploads/5a6077bd4e31a_mm.png', '', '', 'yinpian2', '2018-01-18 18:32:29'),
(240, 0, '/planters-h5/uploads/5a6077ea10ceb_mm.png', '', '', 'yinpian2', '2018-01-18 18:33:14'),
(241, 0, '', '', '/planters-h5/uploads/5a60792d912ab_ne.png', 'yinpian2', '2018-01-18 18:38:37'),
(242, 0, '', '', '/planters-h5/uploads/5a60794b3535d_ne.png', 'yinpian2', '2018-01-18 18:39:07'),
(243, 0, '', '', '/planters-h5/uploads/5a6079980e25f_ne.png', 'yinpian2', '2018-01-18 18:40:24'),
(244, 0, '', '', '/planters-h5/uploads/5a6079d725142_ne.png', 'yinpian2', '2018-01-18 18:41:27'),
(245, 0, '', '', '/planters-h5/uploads/5a607c2a7d658_ne.png', 'yinpian2', '2018-01-18 18:51:22'),
(246, 0, '', '', '/planters-h5/uploads/5a607c63415a3_ne.png', 'yinpian2', '2018-01-18 18:52:19'),
(247, 0, '', '', '/planters-h5/uploads/5a607c972954d_ne.png', 'yinpian2', '2018-01-18 18:53:11'),
(248, 0, '', '', '/planters-h5/uploads/5a607ce37f3be_ne.png', 'yinpian2', '2018-01-18 18:54:27'),
(249, 0, '', '/planters-h5/uploads/5a607d2fe798d_bb.png', '', 'yinpian2', '2018-01-18 18:55:43'),
(250, 0, '', '/planters-h5/uploads/5a607d485de8f_bb.png', '', 'yinpian2', '2018-01-18 18:56:08'),
(251, 0, '', '/planters-h5/uploads/5a607e0a7e795_bb.png', '', 'yinpian2', '2018-01-18 18:59:22'),
(252, 0, '', '/planters-h5/uploads/5a607e5e3abd4_bb.png', '', 'yinpian2', '2018-01-18 19:00:46'),
(253, 0, '/planters-h5/uploads/5a607e9cc0fdc_mm.png', '/planters-h5/uploads/5a607e9cc0fdc_bb.png', '/planters-h5/uploads/5a607e9cc0fdc_ne.png', 'yinpian2', '2018-01-18 19:01:48'),
(254, 0, '/planters-h5/uploads/5a6082d20efc6_mm.png', '/planters-h5/uploads/5a6082d20efc6_bb.png', '/planters-h5/uploads/5a6082d20efc6_ne.png', 'yinpian3', '2018-01-18 19:19:46'),
(255, 0, '/planters-h5/uploads/5a60835d67b38_mm.png', '/planters-h5/uploads/5a60835d67b38_bb.png', '/planters-h5/uploads/5a60835d67b38_ne.png', 'yinpian1', '2018-01-18 19:22:05'),
(256, 0, '/planters-h5/uploads/5a6083dc8c84e_mm.png', '/planters-h5/uploads/5a6083dc8c84e_bb.png', '/planters-h5/uploads/5a6083dc8c84e_ne.png', 'yinpian1', '2018-01-18 19:24:12'),
(257, 0, '/planters-h5/uploads/5a608484cb37f_mm.png', '/planters-h5/uploads/5a608484cb37f_bb.png', '/planters-h5/uploads/5a608484cb37f_ne.png', 'yinpian1', '2018-01-18 19:27:00'),
(258, 0, '/planters-h5/uploads/5a6086132d70a_mm.png', '/planters-h5/uploads/5a6086132d70a_bb.png', '/planters-h5/uploads/5a6086132d70a_ne.png', 'yinpian2', '2018-01-18 19:33:39'),
(259, 0, '/planters-h5/uploads/5a608689f3fd2_mm.png', '/planters-h5/uploads/5a608689f3fd2_bb.png', '/planters-h5/uploads/5a608689f3fd2_ne.png', 'yinpian3', '2018-01-18 19:35:38'),
(260, 0, '/planters-h5/uploads/5a608702f20ec_mm.png', '/planters-h5/uploads/5a608702f20ec_bb.png', '/planters-h5/uploads/5a608702f20ec_ne.png', 'yinpian4', '2018-01-18 19:37:39'),
(261, 0, '', '', '', 'yinpian4', '2018-01-18 19:40:35'),
(262, 0, '/planters-h5/uploads/5a6087ff77420_mm.png', '/planters-h5/uploads/5a6087ff77420_bb.png', '/planters-h5/uploads/5a6087ff77420_ne.png', 'yinpian4', '2018-01-18 19:41:51'),
(263, 0, '/planters-h5/uploads/5a6088d000aa6_mm.png', '/planters-h5/uploads/5a6088d000aa6_bb.png', '/planters-h5/uploads/5a6088d000aa6_ne.png', 'yinpian4', '2018-01-18 19:45:20'),
(264, 0, '/planters-h5/uploads/5a6089c5a529b_mm.png', '/planters-h5/uploads/5a6089c5a529b_bb.png', '/planters-h5/uploads/5a6089c5a529b_ne.png', 'yinpian4', '2018-01-18 19:49:25'),
(265, 0, '/planters-h5/uploads/5a608a1dc0cc5_mm.png', '/planters-h5/uploads/5a608a1dc0cc5_bb.png', '/planters-h5/uploads/5a608a1dc0cc5_ne.png', 'yinpian4', '2018-01-18 19:50:53'),
(266, 0, '', '', '/planters-h5/uploads/5a608a92236fa_ne.png', 'yinpian4', '2018-01-18 19:52:50'),
(267, 0, '', '/planters-h5/uploads/5a608aa962d12_bb.png', '', 'yinpian4', '2018-01-18 19:53:13'),
(268, 0, '', '/planters-h5/uploads/5a608c0016264_bb.png', '', 'yinpian4', '2018-01-18 19:58:56'),
(269, 0, '', '/planters-h5/uploads/5a608c6979c5c_bb.png', '', 'yinpian4', '2018-01-18 20:00:41'),
(270, 0, '', '/planters-h5/uploads/5a608d104a8fe_bb.png', '', 'yinpian4', '2018-01-18 20:03:28'),
(271, 0, '/planters-h5/uploads/5a608e2cc69b8_mm.png', '/planters-h5/uploads/5a608e2cc69b8_bb.png', '/planters-h5/uploads/5a608e2cc69b8_ne.png', 'yinpian4', '2018-01-18 20:08:12'),
(272, 0, '/planters-h5/uploads/5a608e5fd5f18_mm.png', '/planters-h5/uploads/5a608e5fd5f18_bb.png', '/planters-h5/uploads/5a608e5fd5f18_ne.png', 'yinpian4', '2018-01-18 20:09:03'),
(273, 0, '/planters-h5/uploads/5a608f327e8eb_mm.png', '/planters-h5/uploads/5a608f327e8eb_bb.png', '/planters-h5/uploads/5a608f327e8eb_ne.png', 'yinpian4', '2018-01-18 20:12:34');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `yinpian`
--
ALTER TABLE `yinpian`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=60;
--
-- AUTO_INCREMENT for table `yinpian`
--
ALTER TABLE `yinpian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=274;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
