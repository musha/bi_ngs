// Page module
define(["app", "controllers/base/page"],

    function(app, BasePage) {

        var Page = {};

        Page.View = BasePage.View.extend({

            fitOn: "width", //width, height, custom

            beforeRender: function() {
                var done = this.async();

                require(["vendor/greensock/utils/SplitText.min"],
                    function() {
                        done();
                    });
            },

            afterRemove: function() {},
            afterRender: function() {

                var context = this;
                _mwt.push(['_trackPageview', '/h5/yinpian2', 'baocun页面']);
                // switch (app.haibaoIndex) {
                //     case "1":
                //         //设置对应的上传css
                //         app.p_mm = {
                //             position: 'absolute',
                //             overflow: 'hidden',
                //             width: '21%',
                //             height: '19%',
                //             top: '30%',
                //             left: '19%'
                //         }
                //         app.p_bb = {
                //             position: 'absolute',
                //             overflow: 'hidden',
                //             width: '21%',
                //             height: '19%',
                //             top: '30%',
                //             left: '19%'
                //         }
                //         break;
                //     case "2":
                //         //设置对应的上传css
                //         app.p_mm = {
                //             position: 'absolute',
                //             overflow: 'hidden',
                //             width: '22%',
                //             height: '19%',
                //             top: '54%',
                //             left: '46.5%'
                //         }
                //         app.p_bb = {
                //             position: 'absolute',
                //             overflow: 'hidden',
                //             width: '21%',
                //             height: '19%',
                //             top: '59.5%',
                //             left: '26%'
                //         }
                //         $(".page-yinpian1 .mm-container").css(app.p_mm);
                //         $(".page-yinpian1 .bb-container").css(app.p_bb);
                //         break;
                //     case "3":
                //         //设置对应的上传css
                //         app.p_mm = {
                //             position: 'absolute',
                //             overflow: 'hidden',
                //             width: '16.3%',
                //             height: '14%',
                //             top: '45.7%',
                //             left: '51%'
                //         }
                //         app.p_bb = {
                //             position: 'absolute',
                //             overflow: 'hidden',
                //             width: '21.33%',
                //             height: '13.3%',
                //             top: '51.5%',
                //             left: '27%'
                //         }
                //         $(".page-yinpian1 .mm-container").css(app.p_mm);
                //         $(".page-yinpian1 .bb-container").css(app.p_bb);
                //         break;
                //     case "4":
                //         // $(".page-yinpian2 .bg").remove();
                //         // $(".page-yinpian2 .bg").append("<img src='assets/images/yinpian2/sc_4.png'>");
                //         //设置对应的上传css
                //         app.p_mm = {
                //             position: 'absolute',
                //             overflow: 'hidden',
                //             width: '18%',
                //             height: '16%',
                //             top: '61%',
                //             left: '40%'
                //         }
                //         app.p_bb = {
                //             position: 'absolute',
                //             overflow: 'hidden',
                //             width: '26%',
                //             height: '19%',
                //             top: '40.2%',
                //             left: '43%'
                //         }
                //         $(".page-yinpian1 .mm-container").css(app.p_mm);
                //         $(".page-yinpian1 .bb-container").css(app.p_bb);
                //         break;
                //     default:
                //         // $(".page2Bg").append("<img src='assets/images/yinpian1/page2Bg.png'>");
                //         //设置对应的上传css
                //         app.p_mm = {
                //             position: 'absolute',
                //             overflow: 'hidden',
                //             width: '18%',
                //             height: '16%',
                //             top: '60%',
                //             left: '40%'
                //         }
                //         app.p_bb = {
                //             position: 'absolute',
                //             overflow: 'hidden',
                //             width: '26%',
                //             height: '19%',
                //             top: '42%',
                //             left: '43%'
                //         }
                //         $(".page-yinpian1 .mm-container").css(app.p_mm);
                //         $(".page-yinpian1 .bb-container").css(app.p_bb);
                //         break;
                // }
                $(".loadgif").show();
                // $('.baocun').hide();
                setTimeout(function() {
                    // $('.baocun').show();
                    $(".loadgif").hide();
                    // var tl = new TimelineMax({ repeat: 2, yoyo: true });

                    // tl.to($('.baocun'), 0.4, { autoAlpha: 0 }, "+=0.2");

                }, 1500)



                setTimeout(function() {
                    $('.page-yinpian2 .bg').remove();
                    switch (app.haibaoIndex) {
                        case "1":
                            $('.page-yinpian2').append("<img class='bg' src='assets/images/yinpian2/sc_1.png'/>");
                            $('.jt_bg').attr("src", "assets/images/yinpian2/sc_1.png");
                            break;
                        case "2":
                            $('.page-yinpian2').append("<img class='bg' src='assets/images/yinpian2/sc_2.png'/>");
                            $('.jt_bg').attr("src", "assets/images/yinpian2/sc_2.png");
                            break;
                        case "3":
                            $('.page-yinpian2').append("<img class='bg' src='assets/images/yinpian2/sc_3.png'/>");
                            $('.jt_bg').attr("src", "assets/images/yinpian2/sc_3.png");
                            break;
                        case "4":
                            $('.page-yinpian2').append("<img class='bg' src='assets/images/yinpian2/sc_4.png'/>");
                            $('.jt_bg').attr("src", "assets/images/yinpian2/sc_4.png");
                            break;
                        default:
                            $('.page-yinpian2').append("<img class='bg' src='assets/images/yinpian2/sc_1.png'/>");
                            $('.jt_bg').attr("src", "assets/images/yinpian2/sc_1.png");
                            break;
                    }
                }, 200)
                var hh = $(window).height();
                var ww = $(window).width();
                // alert(hh / ww);
                if (hh / ww > 1.65) {

                    $(".page-yinpian2 .showtime").css("bottom", "8.1%");
                    $(".page-yinpian2 .again").css("bottom", "-0.5%");
                } else {
                    $(".page-yinpian2 .showtime").css("bottom", "18%");
                    $(".page-yinpian2 .again").css("bottom", "8%");
                }
                //    $(".page-yinpian2 .showtime").css(); 
                $(".again").click(function() {
                        app.page2url = "";
                        app.page1url = "";
                        app.router.goto("haibao");
                    })
                    // 更改背景
                    // context.$('.bg').attr("src", app.endingImage);
                    // 更改头像
                if (app.mm && app.mm != "") {
                    context.$(".mm img ").attr("src", app.mm);
                } else {
                    context.$(".mm img ").attr("src", app.mmImg)
                }
                if (app.bb && app.bb != "") {
                    context.$(".bb img ").attr("src", app.bb);
                } else {
                    context.$(".bb img ").attr("src", app.bbImg)
                }
                if (app.img_ne && app.img_ne != "") {
                    context.$(".ne img ").attr("src", app.img_ne);
                } else {
                    context.$(".ne img ").attr("src", app.neImg)
                }
                // 更改截图头像
                if (app.mm && app.mm != "") {
                    $(".jt_mm").attr("src", app.mm);
                } else {
                    $(".jt_mm").attr("src", app.mmImg)
                }
                if (app.bb && app.bb != "") {
                    $(".jt_bb").attr("src", app.bb);
                } else {
                    $(".jt_bb").attr("src", app.bbImg)
                }
                if (app.img_ne && app.img_ne != "") {
                    $(".jt_ne").attr("src", app.img_ne);
                } else {
                    $(".jt_ne").attr("src", app.neImg)
                }
                switch (app.haibaoIndex) {
                    case "1":

                        if ($(window).width() == 414) {
                            app.jt_mm = {
                                top: '37.5%',
                                left: '48.9%',
                                transform: 'scale(0.8) rotate(8deg)',
                                width: '22.8%',
                                // height: '16.5%'
                            }
                        } else {
                            app.jt_mm = {
                                top: '37.5%',
                                left: '48.6%',
                                transform: 'scale(0.8) rotate(8deg)',
                                width: '22%',
                                // height: '16.5%'
                            }
                        }
                        if ($(window).width() == 414) {
                            app.jt_bb = {
                                top: '36.8%',

                                left: '12%',
                                width: '34%',
                                transform: 'scale(1)'
                            }
                        } else {
                            app.jt_bb = {
                                top: '37.2%',

                                left: '14%',
                                width: '33%',
                                transform: 'scale(1)'
                            }
                        }
                        break;
                    case "2":
                        app.jt_mm = {
                            top: '45%',
                            left: '45.6%',
                            transform: 'scale(0.8)  rotate(-15deg)',
                            width: '24.2%'
                        }

                        if ($(window).width() == 414) {
                            app.jt_bb = {
                                top: '48%',
                                left: '21%',
                                transform: 'scale(1)',
                                width: '30%',
                                transform: 'rotate(22deg)'
                            }
                        } else {
                            app.jt_bb = {
                                top: '48.7%',
                                left: '21%',
                                transform: 'scale(1)',
                                width: '30%',
                                transform: 'rotate(22deg)'
                            }
                        }
                        break;
                    case "3":
                        if ($(window).width() == 414) {
                            app.jt_mm = {
                                top: '38%',
                                left: '50.6%',
                                transform: 'scale(0.85) rotate(-9deg)',
                                width: "21%"
                            }
                        } else {
                            app.jt_mm = {
                                top: '36.8%',
                                left: '50%',
                                transform: 'scale(0.85) rotate(-9deg)',
                                width: "21%"
                            }
                        }
                        if ($(window).width() == 414) {
                            app.jt_bb = {
                                top: '40.3%',
                                left: '24%',
                                transform: 'scale(1)',
                                width: '31%',
                                transform: 'rotate(12deg)'
                            }
                        } else {
                            app.jt_bb = {
                                top: '39.3%',
                                left: '23%',
                                transform: 'scale(1)',
                                width: '34%',
                                transform: 'rotate(12deg)'
                            }
                        }

                        break;
                    case "4":
                        app.jt_mm = {
                            top: '53%',
                            left: '38.7%',
                            transform: 'scale(0.8)',
                            width: '20%'
                                // height: '16.3%'
                        }


                        if ($(window).width() == 414) {
                            app.jt_bb = {
                                top: '40%',
                                left: '38.5%',
                                transform: 'scale(1)',
                                width: '31%',
                                // height: '19%',
                                transform: 'rotate(11deg)'
                            }
                        } else {
                            app.jt_bb = {
                                top: '38%',
                                left: '37.5%',
                                transform: 'scale(1)',
                                width: '32%',
                                // height: '19%',
                                transform: 'rotate(12deg)'
                            }
                        }
                        break;
                }

                $(".jt_mm").css(app.jt_mm);
                $(".jt_bb").css(app.jt_bb);
                // 更改截图背景  
                // 更改头像样式
                switch (app.haibaoIndex) {
                    case "1":
                        if ($(window).width() == 414) {
                            app.bbCss = {
                                top: '38.5%',
                                left: '8%',
                                'z-index': '1',
                                transform: 'scale(0.8)',
                                width: '43%',
                                height: '17.2%'
                            }
                        } else {
                            app.bbCss = {
                                top: '38.5%',
                                left: '8.1%',
                                'z-index': '1',
                                transform: 'scale(0.8)',
                                width: '43%',
                                height: '17.2%'
                            }
                        }
                        app.mmCss = {
                            top: '41.8%',
                            left: '50%',
                            transform: 'scale(0.78) rotate(9deg)',
                            width: '24%',
                            // height: '17.6%'
                        }
                        break;
                    case "2":

                        if ($(window).width() == 414) {
                            app.bbCss = {
                                top: '52.5%',
                                left: '25%',
                                'z-index': '1',
                                transform: 'scale(1.2)',
                                width: '30.3%',
                                transform: 'rotate(22deg)',
                                height: '13%'
                            }
                        } else {
                            app.bbCss = {
                                top: '52.5%',
                                left: '25%',
                                'z-index': '1',
                                transform: 'scale(1.2)',
                                width: '30.3%',
                                transform: 'rotate(22deg)',
                                height: '13%'
                            }
                        }
                        if ($(window).width() == 414) {
                            app.mmCss = {
                                top: '50.7%',
                                left: '41%',
                                transform: 'scale(0.7)  rotate(-15deg)',
                                // height: '24%',
                                width: '27.6%'
                            }
                        } else {
                            app.mmCss = {
                                top: '50.5%',
                                left: '40.6%',
                                transform: 'scale(0.7)  rotate(-15deg)',
                                // height: '24%',
                                width: '27.6%'
                            }
                        }
                        break;
                    case "3":
                        if ($(window).width() == 414) {
                            app.bbCss = {
                                position: 'absolute',
                                overflow: 'hidden',
                                width: '33%',
                                height: '19%',
                                top: '43.5%',
                                left: '24%',
                                'z-index': '1',
                                transform: 'rotate(12deg)'
                            }
                        } else {
                            app.bbCss = {
                                position: 'absolute',
                                overflow: 'hidden',
                                width: '33%',
                                height: '19.5%',
                                top: '43.5%',
                                left: '24.8%',
                                'z-index': '1',
                                transform: 'rotate(12deg)'
                            }
                        }
                        app.mmCss = {
                            position: 'absolute',
                            overflow: 'hidden',
                            width: '20%',
                            height: '20%',
                            top: '41.1%',
                            left: '51%',
                            'transform': 'scale(0.85)  rotate(-9deg)'
                        }
                        break;
                    case "4":

                        if ($(window).width() == 414) {
                            app.bbCss = {
                                top: '41.8%',
                                left: '39%',
                                'z-index': '1',
                                transform: 'scale(1)',
                                width: '34.3%',
                                height: '13.2%',
                                transform: 'rotate(11deg)'
                            }
                        } else {
                            app.bbCss = {
                                top: '41.1%',
                                left: '38%',
                                'z-index': '1',
                                transform: 'scale(1)',
                                width: '34.3%',
                                height: '13.2%',
                                transform: 'rotate(11deg)'
                            }
                        }
                        app.mmCss = {
                            top: '57%',
                            left: '38.7%',
                            transform: 'scale(0.86)',
                            width: '20%',
                            height: '18.3%'
                        }
                        break;
                }

                context.$(".bb").css(app.bbCss);
                context.$(".mm").css(app.mmCss);
                // 截屏功能
                $('.jt').show();
                var jtTl = setTimeout(function() {
                    html2canvas($('.jt'), {
                        onrendered: function(canvas) {
                            context.$('.wrapper1').html('<img src="' + canvas.toDataURL("image/png", 1.0) + '" />');
                            $('.jt').hide();
                            canvas = null;
                        }
                    });
                }, 2000);
            }
        });

        // Return the module for AMD compliance.
        return Page;

    })