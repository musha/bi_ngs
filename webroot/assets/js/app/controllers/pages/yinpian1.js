/**
 * Created by DELL on 2017/11/1.
 */
// Page module
define(["app", "controllers/base/page"],

    function(app, BasePage) {
        var Page = {};

        Page.View = BasePage.View.extend({
            fitOn: "width", //width, height, custom

            beforeRender: function() {

                var done = this.async();

                require(["vendor/zepto/zepto.html5Loader.min"],
                    function() {
                        done();
                    });
            },
            beforeRender: function() {},
            is_img_bb: false,
            is_img_mm: false,
            afterRender: function() {
                _mwt.push(['_trackPageview', '/h5/3', '03确认生成页']);
                app.img_bb = "";
                app.img_mm = "";
                var context = this;
                // $(".page2Bg").attr("src", "assets/images/yinpian1/page2Bg2.png");
                if (app.showtext) {
                    setTimeout(function() {
                            var tl = new TimelineMax({ repeat: 2, yoyo: true });
                            tl.to($('.showtext'), 0.3, { autoAlpha: 0 }, "+=0.2");
                            // var tl = new TimelineMax({ repeat: 0, yoyo: true });
                            // tl.to($('.showtext'), 0.1, { autoAlpha: 0 }, "+=0.1");

                            // $('.showtext').hide();
                        }, 700)
                        // setTimeout(function() {
                        //     $('.showtext').hide();
                        //    
                        // }, 800)

                } else {
                    $('.showtext').hide();
                }

                switch (app.haibaoIndex) {
                    case "1":
                        $(".addPicFather").css("top", "44%");
                        $(".addPicFather").css("left", "7.5%");
                        $(".addPic").css("top", "46.6%");
                        $(".addPic").css("left", "69%");
                        $(".page2Bg").attr("src", "assets/images/yinpian1/page2Bg.png");
                        // $(".page2Bg").append("<img src='assets/images/yinpian1/page2Bg.png'>");
                        //设置对应的上传css

                        if ($(window).width() == 414) {
                            app.p_mm = {
                                position: 'absolute',
                                overflow: 'hidden',
                                width: '20%',
                                height: '19.2%',
                                top: '41.6%',
                                left: '49.6%',
                                // 'z-index': '999',
                                'transform': 'scale(0.88) rotate(9deg)'
                                    // opacity: '0.4'
                            }
                        } else {
                            app.p_mm = {
                                position: 'absolute',
                                overflow: 'hidden',
                                width: '20%',
                                height: '19.2%',
                                top: '41%',
                                left: '49.6%',
                                // 'z-index': '999',
                                'transform': 'scale(0.88) rotate(9deg)'
                                    // opacity: '0.4'
                            }
                        }
                        if ($(window).width() == 414) {
                            app.p_bb = {
                                position: 'absolute',
                                overflow: 'hidden',
                                width: '29%',
                                height: '19%',
                                top: '42%',
                                left: '15.5%'
                            }
                        } else {
                            app.p_bb = {
                                position: 'absolute',
                                overflow: 'hidden',
                                width: '28%',
                                height: '19%',
                                top: '43%',
                                left: '16.5%'
                            }
                        }
                        $(".page-yinpian1 .mm-container").css(app.p_mm);
                        $(".page-yinpian1 .bb-container").css(app.p_bb);
                        break;
                    case "2":
                        $(".addPicFather").css("top", "43%");
                        $(".addPicFather").css("left", "15.5%");
                        $(".addPic").css("top", "56.6%");
                        $(".addPic").css("left", "69%");
                        $(".page2Bg").attr("src", "assets/images/yinpian1/page2Bg2.png");
                        // $(".page2Bg").append("<img src='assets/images/yinpian1/page2Bg2.png'>");
                        //设置对应的上传css
                        app.p_mm = {
                            position: 'absolute',
                            overflow: 'hidden',
                            width: '25%',
                            height: '24%',
                            top: '47.7%',
                            left: '46%',
                            'transform': 'scale(0.75) rotate(-15deg)'
                        }

                        if ($(window).width() == 414) {
                            app.p_bb = {
                                position: 'absolute',
                                overflow: 'hidden',
                                width: '25%',
                                height: '19%',
                                top: '54%',
                                left: '25.2%',
                                transform: 'rotate(17deg)'
                            }
                        } else {
                            app.p_bb = {
                                position: 'absolute',
                                overflow: 'hidden',
                                width: '25%',
                                height: '19%',
                                top: '54%',
                                left: '24.6%',
                                transform: 'rotate(17deg)'
                            }
                        }
                        $(".page-yinpian1 .mm-container").css(app.p_mm);
                        $(".page-yinpian1 .bb-container").css(app.p_bb);
                        break;
                    case "3":
                        $(".addPicFather").css("top", "52%");
                        $(".addPicFather").css("left", "15.5%");
                        $(".addPic").css("top", "50.6%");
                        $(".addPic").css("left", "68%");
                        $(".page2Bg").attr("src", "assets/images/yinpian1/page2Bg3.png");
                        // $(".page2Bg").append("<img src='assets/images/yinpian1/page2Bg3.png'>");
                        //设置对应的上传css
                        if ($(window).width() == 414) {
                            app.p_mm = {
                                position: 'absolute',
                                overflow: 'hidden',
                                width: '19.8%',
                                height: '17%',
                                top: '41.5%',
                                left: '51.5%',
                                'transform': 'scale(0.85)  rotate(-13deg)'
                            }
                        } else {
                            app.p_mm = {
                                position: 'absolute',
                                overflow: 'hidden',
                                width: '21%',
                                height: '17%',
                                top: '40%',
                                left: '50%',
                                'transform': 'scale(0.85)  rotate(-13deg)'
                            }
                        }

                        if ($(window).width() == 414) {
                            app.p_bb = {
                                position: 'absolute',
                                overflow: 'hidden',
                                width: '28%',
                                height: '19%',
                                top: '45%',
                                left: '26.5%',
                                transform: 'rotate(10deg)'
                            }
                        } else {
                            app.p_bb = {
                                position: 'absolute',
                                overflow: 'hidden',
                                width: '28%',
                                height: '19%',
                                top: '45.6%',
                                left: '27.5%',
                                transform: 'rotate(10deg)'
                            }
                        }
                        $(".page-yinpian1 .mm-container").css(app.p_mm);
                        $(".page-yinpian1 .bb-container").css(app.p_bb);
                        break;
                    case "4":
                        $(".addPicFather").css("top", "39%");
                        $(".addPicFather").css("left", "29.5%");
                        $(".addPic").css("top", "60.6%");
                        $(".addPic").css("left", "58%");
                        $(".page2Bg").attr("src", "assets/images/yinpian1/page2Bg4.png");
                        // $(".page2Bg").append("<img src='assets/images/yinpian1/page2Bg4.png'>");
                        //设置对应的上传css

                        if ($(window).width() == 414) {
                            app.p_mm = {
                                position: 'absolute',
                                overflow: 'hidden',
                                width: '18.5%',
                                // height: '18.3%',
                                top: '58.15%',
                                left: '39%',
                                'transform': 'scale(0.88) '
                            }
                        } else {
                            app.p_mm = {
                                position: 'absolute',
                                overflow: 'hidden',
                                width: '18.5%',
                                // height: '18.3%',
                                top: '58%',
                                left: '39%',
                                'transform': 'scale(0.88) '
                            }
                        }


                        if ($(window).width() == 414) {
                            app.p_bb = {
                                position: 'absolute',
                                overflow: 'hidden',
                                width: '29%',
                                height: '19%',
                                top: '44.5%',
                                left: '41.5%',
                                transform: 'rotate(12deg)'
                            }

                        } else {
                            app.p_bb = {
                                position: 'absolute',
                                overflow: 'hidden',
                                width: '28%',
                                height: '19%',
                                top: '44%',
                                left: '41.5%',
                                transform: 'rotate(12deg)'
                            }

                        }
                        $(".page-yinpian1 .mm-container").css(app.p_mm);
                        $(".page-yinpian1 .bb-container").css(app.p_bb);
                        break;
                    default:
                        $(".page2Bg").append("<img src='assets/images/yinpian1/page2Bg.png'>");
                        //设置对应的上传css
                        app.p_mm = {
                            position: 'absolute',
                            overflow: 'hidden',
                            width: '18%',
                            height: '16%',
                            top: '60%',
                            left: '40%'
                        }
                        app.p_bb = {
                            position: 'absolute',
                            overflow: 'hidden',
                            width: '26%',
                            height: '19%',
                            top: '42%',
                            left: '43%'
                        }
                        $(".page-yinpian1 .mm-container").css(app.p_mm);
                        $(".page-yinpian1 .bb-container").css(app.p_bb);
                        break;

                }

                function loadgifhide() {
                    $(".loadgif").show();
                    setTimeout(function() {
                        $(".loadgif").hide();
                    }, 2000)
                }
                if (app.haibaoIndex == "1") {
                    $(".yinpian1 .page2Bg").attr("src", "assets/images/yinpian1/page2Bg.png");
                    setTimeout(function() {
                        if (!app.page2url == "") {
                            if (app.page1url == "") {
                                $(".yinpian1 .page2Bg").attr("src", "assets/images/haibao/p1_new.png");
                                loadgifhide();
                            } else {
                                $(".yinpian1 .page2Bg").attr("src", "assets/images/haibao/p1_new_all.png");
                                loadgifhide();
                            }
                        } else {
                            if (!app.page1url == "") {
                                $(".yinpian1 .page2Bg").attr("src", "assets/images/haibao/p1_new_ren.png");
                                loadgifhide();
                            }
                        }
                    }, 500)
                }
                if (app.haibaoIndex == "2") {
                    $(".yinpian1 .page2Bg").attr("src", "assets/images/yinpian1/page2Bg2.png");
                    setTimeout(function() {
                        if (!app.page2url == "") {
                            if (app.page1url == "") {
                                $(".yinpian1 .page2Bg").attr("src", "assets/images/haibao/p2_new.png");
                                loadgifhide();
                            } else {
                                $(".yinpian1 .page2Bg").attr("src", "assets/images/haibao/p2_new_all.png");
                                loadgifhide();
                            }
                        } else {
                            if (!app.page1url == "") {
                                $(".yinpian1 .page2Bg").attr("src", "assets/images/haibao/p2_new_ren.png");
                                loadgifhide();
                            }
                        }
                    }, 500)
                }
                if (app.haibaoIndex == "3") {
                    $(".yinpian1 .page2Bg").attr("src", "assets/images/yinpian1/page2Bg3.png");
                    setTimeout(function() {
                        if (!app.page2url == "") {
                            if (app.page1url == "") {
                                $(".yinpian1 .page2Bg").attr("src", "assets/images/haibao/p3_new.png");
                                loadgifhide();
                            } else {
                                $(".yinpian1 .page2Bg").attr("src", "assets/images/haibao/p3_new_all.png");
                                loadgifhide();
                            }
                        } else {
                            if (!app.page1url == "") {
                                $(".yinpian1 .page2Bg").attr("src", "assets/images/haibao/p3_new_ren.png");
                                loadgifhide();
                            }
                        }
                    }, 500)
                }
                if (app.haibaoIndex == "4") {
                    $(".yinpian1 .page2Bg").attr("src", "assets/images/yinpian1/page2Bg4.png");
                    setTimeout(function() {
                        if (!app.page2url == "") {
                            if (app.page1url == "") {
                                $(".yinpian1 .page2Bg").attr("src", "assets/images/haibao/p4_new.png");
                                loadgifhide();
                            } else {
                                $(".yinpian1 .page2Bg").attr("src", "assets/images/haibao/p4_new_all.png");
                                loadgifhide();
                            }
                        } else {
                            if (!app.page1url == "") {
                                $(".yinpian1 .page2Bg").attr("src", "assets/images/haibao/p4_new_ren.png");
                                loadgifhide();
                            }
                        }
                    }, 500)
                }

                context.$(".again").click(function() {
                    app.router.goto("haibao");
                })
                context.$(".create").click(function() {
                        // _mwt.push(['_trackEvent', '确认生成', 01]);
                        gtag('event', '确认生成', {
                            'event_category': 'querenshengcheng',
                            'event_label': 'click'
                        });
                        app.mm = context.$(".mm img")[0].getAttribute("src");
                        app.bb = context.$(".bb img")[0].getAttribute("src");
                        app.router.goto("yinpian2");

                    })
                    // alert($(".page2Bg")[0]);
                if (app.page2url) {
                    context.loadImage(app.page2url, "bb");
                }
                if (app.page1url) {
                    context.loadImage(app.page1url, "mm");
                }
                // 返回按钮
                context.$('.return_bt').click(function() {
                    app.router.goto('haibao');
                    app.tracker.event('click', 'event', 'yinpian1_returnBt');
                })
                var isLoading = false;
                context.$(".kaichangBtn").click(function() {
                    if (isLoading) {
                        return;
                    }
                    isLoading = true;

                    $('#bg_bak_in_2s_out_2s').remove();
                    var img_bb = "";
                    var img_mm = "";
                    // 头像截图
                    html2canvas($(".bb-container-div"), {
                        onrendered: function(canvas) {
                            var url = canvas.toDataURL("image/png", 1.0);
                            //var img = new Image();
                            //img.src = url; 
                            //document.body.append(img); 
                            img_bb = url;
                            html2canvas($(".mm-container-div"), {
                                onrendered: function(canvas) {
                                    var url = canvas.toDataURL("image/png", 1.0);
                                    img_mm = url;

                                }
                            });
                        }
                    });
                });

                //判断机型
                var ua = navigator.userAgent.toLowerCase();
                var isIos = (ua.indexOf('iphone') != -1) || (ua.indexOf('ipad') != -1);
                if (isIos) {
                    $("#file").removeAttr("capture");
                    $("#fileFather").removeAttr("capture");
                    $("#fileMe").removeAttr("capture");
                };
                context.$(".huodongxize").click(function() {
                    app.router.goto("layer");

                });
                //上传图片

                var headPic = 'bb';

                $(".reupload1, .reupload2, .reupload3").hide();
                // bb
                context.$(".addPicFather, .reupload2").on("click", function() {
                    // headPic = 'bb';
                    app.router.goto("gou");
                    // $("#file").trigger("click");

                    // // $(".addPicFather, .blackPic2, .divPic2").remove();

                    // $(".reupload2").show();
                    // context.is_img_bb = true; 
                });

                // mm
                context.$(".addPic, .reupload1").on("click", function() {
                    app.router.goto("ren");
                    // headPic = 'mm';
                    // $("#file").trigger("click");

                    // // $(".addPic, .blackPic, .divPic1").remove();

                    // $(".reupload1").show();

                    // context.is_img_mm = true; 
                });
                context.$("#file").on("change", function() {
                    getObjectURL(this.files[0], headPic);
                });
                // context.$("#file").on("change", function() {
                //     getObjectURL(this.files[0]);
                // });
                //获取上传图片的url
                function getObjectURL(file, headPic) {

                    context.$("#file")[0].value = null;

                    var reader = new FileReader();
                    console.log(1);

                    EXIF.getData(file, function() {
                        EXIF.getAllTags(this);
                        Orientation = EXIF.getTag(this, 'Orientation');
                        console.log(2);
                    });
                    reader.readAsDataURL(file); //转base64
                    reader.onload = function(e) {
                        console.log(3);
                        var image = new Image();
                        image.src = e.target.result;
                        image.onload = function() {
                            console.log('canvas');
                            var expectWidth = this.naturalWidth;
                            var expectHeight = this.naturalHeight;

                            if (this.naturalWidth > this.naturalHeight && this.naturalWidth > 800) {
                                expectWidth = 800;
                                expectHeight = expectWidth * this.naturalHeight / this.naturalWidth;
                            } else if (this.naturalHeight > this.naturalWidth && this.naturalHeight > 1200) {
                                expectHeight = 1200;
                                expectWidth = expectHeight * this.naturalWidth / this.naturalHeight;
                            }
                            var canvas = document.createElement("canvas");
                            var ctx = canvas.getContext("2d");
                            canvas.width = expectWidth;
                            canvas.height = expectHeight;
                            ctx.drawImage(this, 0, 0, expectWidth, expectHeight);

                            if (Orientation !== "" && Orientation !== 1) {
                                console.log("需要旋转")
                                switch (Orientation) {
                                    case 6: //需要顺时针（向左）90度旋转
                                        context.rotateImg(this, 'left', canvas);
                                        break;
                                    case 8: //需要逆时针（向右）90度旋转
                                        context.rotateImg(this, 'right', canvas);
                                        break;
                                    case 3: //需要180度旋转
                                        context.rotateImg(this, 'right', canvas); //转两次
                                        context.rotateImg(this, 'right', canvas);
                                        break;
                                }
                            }
                            context.$(".one").hide();
                            context.$(".two").show();
                            context.loadImage(canvas.toDataURL("image/jpeg", 0.8), headPic);


                        };
                    };
                }
            },

            rotateImg: function(img, direction, canvas) {
                console.log(4)
                    //最小与最大旋转方向，图片旋转4次后回到原方向
                var min_step = 0;
                var max_step = 3;
                //var img = document.getElementById(pid);
                if (img == null) return;
                //img的高度和宽度不能在img元素隐藏后获取，否则会出错
                var height = img.height;
                var width = img.width;
                //var step = img.getAttribute('step');
                var step = 2;
                if (step == null) {
                    step = min_step;
                }
                if (direction === 'right') {
                    step++;
                    //旋转到原位置，即超过最大值
                    step > max_step && (step = min_step);
                } else {
                    step--;
                    step < min_step && (step = max_step);
                }
                //旋转角度以弧度值为参数
                var degree = step * 90 * Math.PI / 180;
                var ctx = canvas.getContext('2d');
                switch (step) {
                    case 0:
                        canvas.width = width;
                        canvas.height = height;
                        ctx.drawImage(img, 0, 0);
                        break;
                    case 1:
                        canvas.width = height;
                        canvas.height = width;
                        ctx.rotate(degree);
                        ctx.drawImage(img, 0, -height);
                        break;
                    case 2:
                        canvas.width = width;
                        canvas.height = height;
                        ctx.rotate(degree);
                        ctx.drawImage(img, -width, -height);
                        break;
                    case 3:
                        canvas.width = height;
                        canvas.height = width;
                        ctx.rotate(degree);
                        ctx.drawImage(img, -width, 0);
                        break;
                }
            },

            interact_js: function(scaleElement, scale, _tx, _ty, anrolate) {
                var context = this,
                    gestureArea = document.getElementById('gesture-area'),
                    angle1 = anrolate;
                // target elements with the "draggable" class
                interact(gestureArea)
                    .gesturable({
                        onstart: function(event) {

                            scaleElement.classList.remove('reset');
                            angle1 += event.da;
                            ans = event.ds;
                        },
                        onmove: function(event) {
                            var scale1 = scale * (1 + ans);
                            angle1 += event.da;
                            //alert(angle1)
                            ans = event.ds;

                            console.log(_tx + "w000")

                            scaleElement.style.webkitTransform =
                                scaleElement.style.transform =
                                'translate(' + _tx + 'px, ' + _ty + 'px) scale(' + scale1 + ') rotate(' + angle1 + 'deg)';

                            $(scaleElement).find("a").show();


                            if (scale < 0.5) {
                                $(scaleElement).find("a").css("margin-top", -$(scaleElement).width() * 0.3);
                                $(scaleElement).find("a").css("width", $(scaleElement).width() * (1 - scale));
                            } else {
                                $(scaleElement).find("a").css("margin-top", -$(scaleElement).width() * 0.13);
                                $(scaleElement).find("a").css("width", $(scaleElement).width() * 0.3);
                            }


                            anrolate = angle1;
                            scale = scale1;
                            $(scaleElement).attr("scalerle", scale1);
                            $(scaleElement).attr("anrolate", anrolate);
                            context.dragMoveListener(event, scaleElement, scale, _tx, _ty, anrolate);
                        },
                        onend: function(event) {
                            var arrow = event.target;
                            //resetTimeout = setTimeout(context.reset, 1000);
                            scaleElement.classList.add('reset');
                            $(scaleElement).find("a").show();

                            if (scale < 0.5) {
                                $(scaleElement).find("a").css("margin-top", -$(scaleElement).width() * 0.3);
                                $(scaleElement).find("a").css("width", $(scaleElement).width() * (1 - scale))
                            } else {
                                $(scaleElement).find("a").css("margin-top", -$(scaleElement).width() * 0.13);
                                $(scaleElement).find("a").css("width", $(scaleElement).width() * 0.3);
                            }


                        }
                    }).draggable({ // enable inertial throwing
                        inertia: true,
                        // keep the element within the area of it's parent
                        restrict: {
                            restriction: "parent",
                            endOnly: true,
                            // elementRect: { top: 0, left: 0, bottom: 1, right: 1 }
                        },
                        // enable autoScroll
                        autoScroll: true,

                        // call this function on every dragmove event
                        onmove: function(event) {

                            console.log("onmove");

                            context.dragMoveListener(event, scaleElement, scale, _tx, _ty, anrolate);
                        },
                        // call this function on every dragend event

                        onend: function(event) {

                        }
                    })
                    .resizable({
                        preserveAspectRatio: true,
                        edges: { left: true, right: true, bottom: true, top: true },
                        restrictSize: {
                            min: { width: 300, height: 300 },
                        }
                    })
                    .on('touchstart', function(event) {
                        // var target = event.target;
                        event.stopPropagation();
                        context.$(".cav .draggable").css("border", "none");
                        context.$(".cav .draggable a").hide();
                        context.$(".cav .draggable .zhuan").hide();
                        // alert(scaleElement)
                        $(scaleElement).css("border", "1px dashed #fff");
                        $(scaleElement).find("a").show();
                        $(scaleElement).find(".zhuan").show();
                        if (scale < 0.5) {
                            $(scaleElement).find("a").css("margin-top", -$(scaleElement).width() * 0.3);
                            $(scaleElement).find("a").css("width", $(scaleElement).width() * (1 - scale))
                        } else {
                            $(scaleElement).find("a").css("margin-top", -$(scaleElement).width() * 0.13);
                            $(scaleElement).find("a").css("width", $(scaleElement).width() * 0.3);
                        }
                    })


                // this is used later in the resizing and gesture demos
                window.dragMoveListener = context.dragMoveListener;
            },
            dragMoveListener: function(event, scaleElement, scale, _tx, _ty, anrolate) {
                // keep the dragged position in the data-x/data-y attributes
                var x = (parseFloat(scaleElement.getAttribute('data-x')) || 0) + event.dx,
                    y = (parseFloat(scaleElement.getAttribute('data-y')) || 0) + event.dy;
                _tx = x;
                _ty = y;
                // translate the element
                scaleElement.style.webkitTransform =
                    scaleElement.style.transform =
                    'translate(' + x + 'px, ' + y + 'px) scale(' + scale + ') rotate(' + anrolate + 'deg)';

                // update the posiion attributes
                scaleElement.setAttribute('data-x', x);
                scaleElement.setAttribute('data-y', y);
            },
            drableTouch: function(headPic) {
                var context = this;
                context.$("." + headPic).off("touchstart");
                context.$("." + headPic).each(function() {
                    $(this).on("touchstart", function() {

                        scaleElement = this;
                        var _scale = $(this).attr("scalerle");
                        var _anrolate = $(this).attr("anrolate");
                        if (parseFloat(_scale) == 0) {
                            _scale = 1;
                        } else {
                            $(this).attr("scalerle", _scale);
                            _scale = parseFloat(_scale);
                        }

                        if (parseFloat(_anrolate) == 0) {
                            _anrolate = 0;
                        } else {
                            $(this).attr("anrolate", _anrolate);
                            _anrolate = parseFloat(_anrolate);
                        }
                        var _tx = $(this).attr("data-x");
                        var _ty = $(this).attr("data-y");
                        console.log(_tx);
                        console.log(_ty)
                            // $(this).off("touchstart");
                        context.interact_js(this, _scale, _tx, _ty, _anrolate);

                    })
                })
            },
            loadImage: function(url, headPic) {
                var context = this;

                $("." + headPic).remove();
                context.$(".cav ." + headPic + "-container div").append('<div style="position:absolute;" class="' + headPic + ' f1_l draggable" scalerle="0" anrolate="0"><img width="100%"   class="' + headPic + '" src="' + url + '" alt="" ></a></div>');
                $(".bb").width("110%");
                $(".bb").css("margin-left", "-7%");
                $(".bb").css("margin-top", "-7%");
                // context.drableTouch(headPic);

                $("." + headPic).trigger("touchstart");

            },
            cavTouch: function(event) {

            },
            // 条转处理
            complete: function() {
                var context = this;
                // console.log(this.html);
                // if (app.share_dance_id) {

                //     app.share_dance_id = null;

                //     app.router.goto('page1');
                // } else {
                //     app.router.goto('ending');
                // }


                // 更改背景
                app.endingImage = "assets/images/ending/yp1_1.png";

                // 更改截图背景
                app.jtImage = "assets/images/ending/yp1_2.png";
                // $('.jt_bg').attr("src", app.jtImage);


                app.jtBBcss = {

                    top: '14.7%',
                    left: '15.5%',
                    transform: 'scale(0.72)',

                }
                app.jtMMcss = {

                    top: '12.5%',
                    left: '67.6%',
                    transform: 'scale(0.7)',
                }
                app.jtNEcss = {

                    top: '8%',
                    left: '36.5%',
                    transform: 'scale(0.75)',

                }
                $('.jt_bb').css(app.jtBBcss);
                $('.jt_mm').css(app.jtMMcss);
                $('.jt_ne').css(app.jtNEcss);


                // 更改头像样式
                // app.bbImg = app.img_bb;
                // app.mmImg = app.img_mm;


                app.bbCss = {

                    top: '17.5%',
                    left: '18.7%',
                    transform: 'scale(0.7)',
                };
                app.mmCss = {

                    top: '15.8%',
                    left: '63.6%',
                    transform: 'scale(0.66)',
                };
                app.neCss = {

                    top: ' 11.4%',
                    left: ' 36.5%',
                    transform: ' scale(0.65)',
                };


            }

        });
        // Return the module for AMD compliance.
        return Page;
    });