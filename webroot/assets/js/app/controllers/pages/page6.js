// Page module
define(["app", "controllers/base/page"],

    function(app, BasePage) {

        var Page = {};

        Page.View = BasePage.View.extend({
            fitOn: "width", //width, height, custom
            beforeRender: function() {
                var done = this.async();
                require(["vendor/greensock/utils/SplitText.min"],
                    function() {
                        done();
                    });
            },
            afterRender: function() {
                var context = this;
                _mwt.push(['_trackPageview', '/h5/page6', 'page6']);
                // 主体动画
                $('.skip').css('top', $(window).height() * 0.9);
                app.skip_arrow();
                var tl = new TimelineMax();
                context._t = tl;
                tl.from(context.$('.page6'), 0.4, {
                    autoAlpha: 0,
                    onComplete: function() {
                        drool();
                        pawRubbing();
                        heartTwinkle();
                        tailShake(context.$('.tail1'), 14);
                    }
                }, 0.3);
                tl.to(context.$('.hand1'), 0.2, {
                    onStart: function() {
                        handShow(context.$('.hand1'), 1);
                    }
                }, '-=0.1');
                tl.to(context.$('.dialog1'), 0.1, {
                    autoAlpha: 1,
                    onComplete: function() {
                        page6Play('pg6_mic1');
                        dialog1();
                    }
                }, '+=0.5');
                tl.to(context.$('.dialog1'), 0.1, { autoAlpha: 0 }, '+=3');
                tl.to(context.$('.dialog2'), 0.1, {
                    autoAlpha: 1,
                    onComplete: function() {
                        dialog2();
                    }
                });
                // 场景2
                tl.to(context.$('.scene1'), 0.2, { autoAlpha: 0 }, '+=5.4');
                tl.to(context.$('.scene2'), 0.2, { autoAlpha: 1 }, '-=0.1');
                tl.to(context.$('.hand2'), 0.2, {
                    onStart: function() {
                        handShow(context.$('.hand2'), 2);
                    }
                }, '-=0.1');
                tl.to(context.$('.dialog3'), 0.1, {
                    autoAlpha: 1,
                    onComplete: function() {
                        page6Play('pg6_mic2');
                        dialog3();
                        eyesTwinkle();
                        blueLight();
                        tailShake(context.$('.tail2'), 18);
                    }
                });
                tl.to(context.$('.dialog3'), 0.1, { autoAlpha: 0 }, '+=3.8');
                tl.to(context.$('.dialog4'), 0.1, {
                    autoAlpha: 1,
                    onComplete: function() {
                        dialog4();
                    }
                }, '-=0.1');
                tl.to(context.$('.dialog4'), 0.1, { autoAlpha: 0 }, '+=1.5');
                tl.to(context.$('.dialog5'), 0.1, {
                    autoAlpha: 1,
                    onComplete: function() {
                        dialog5();
                    }
                }, '-=0.1');
                // 场景3
                tl.to(context.$('.scene2'), 0.2, { autoAlpha: 0 }, '+=7');
                tl.to(context.$('.scene3'), 0.2, { autoAlpha: 1 }, '-=0.1');
                tl.to(context.$('.hand3'), 0.2, {
                    onStart: function() {
                        handShow(context.$('.hand3'), 3);
                    }
                }, '-=0.1');
                tl.to(context.$('.dialog6'), 0.1, {
                    autoAlpha: 1,
                    onComplete: function() {
                        page6Play('pg6_mic3');
                        dialog6();
                        dance();
                    }
                });
                tl.to(context.$('.dialog6'), 0.1, { autoAlpha: 0 }, '+=3.5');
                tl.to(context.$('.dialog7'), 0.1, {
                    autoAlpha: 1,
                    onComplete: function() {
                        dialog7();
                    }
                }, '-=0.1');
                tl.to(context.$('.page6'), 0.1, {
                    onComplete: function() {
                        tl.kill();
                        app.router.goto('end');
                    }
                }, '+=6.5');

                // skip
                var current6Mp3;
                $('.skip').on('click', function() {
                    if (current6Mp3 != undefined) {
                        current6Mp3.get(0).pause();
                    }
                    tl.kill();
                    app.router.goto('end');
                });

                // 第一段文字
                function dialog1() {
                    var tl = new TimelineMax();
                    tl.to(context.$('.text1-1'), 0.15, { scale: 2, repeat: 1, yoyo: true }, '+=0.5');
                    tl.to(context.$('.text1-2'), 0.15, { scale: 2, repeat: 1, yoyo: true }, '+=1.2');
                    tl.to(context.$('.text1-3'), 0.15, { scale: 2, repeat: 1, yoyo: true }, '-=0.1');
                    tl.to(context.$('.text1-4'), 0.15, { scale: 2, repeat: 1, yoyo: true });
                    tl.to(context.$('.text1-5'), 0.15, { scale: 2, repeat: 1, yoyo: true }, '-=0.1');
                };

                // 第二段文字
                function dialog2() {
                    var tl = new TimelineMax();
                    tl.to(context.$('.text2-1'), 0.15, { scale: 2, repeat: 1, yoyo: true }, '+=0.3');
                    tl.to(context.$('.text2-2'), 0.15, { scale: 2, repeat: 1, yoyo: true }, '+=0.5');
                    tl.to(context.$('.text2-3'), 0.15, { scale: 2, repeat: 1, yoyo: true }, '+=0.5');
                    tl.to(context.$('.text2-4'), 0.15, { scale: 2, repeat: 1, yoyo: true }, '-=0.1');
                    tl.to(context.$('.text2-5'), 0.15, { scale: 2, repeat: 1, yoyo: true }, '+=0.9');
                    tl.to(context.$('.text2-6'), 0.15, { scale: 2, repeat: 1, yoyo: true }, '-=0.1');
                    tl.to(context.$('.text2-7'), 0.15, { scale: 2, repeat: 1, yoyo: true }, '+=1.4');
                    tl.to(context.$('.text2-8'), 0.2, { scale: 2, repeat: 1, yoyo: true }, '-=0.1');
                    tl.to(context.$('.text2-9'), 0.19, { scale: 2, repeat: 1, yoyo: true });
                    tl.to(context.$('.text2-10'), 0.19, { scale: 2, repeat: 1, yoyo: true });
                };

                // 第三段文字
                function dialog3() {
                    var tl = new TimelineMax();
                    tl.to(context.$('.text3-1'), 0.15, { scale: 2, repeat: 1, yoyo: true }, '+=1');
                    tl.to(context.$('.text3-2'), 0.15, { scale: 2, repeat: 1, yoyo: true }, '+=1');
                    tl.to(context.$('.text3-3'), 0.15, { scale: 2, repeat: 1, yoyo: true }, '-=0.1');
                    tl.to(context.$('.text3-4'), 0.15, { scale: 2, repeat: 1, yoyo: true }, '-=0.1');
                    tl.to(context.$('.text3-5'), 0.15, { scale: 2, repeat: 1, yoyo: true }, '-=0.1');
                    tl.to(context.$('.text3-6'), 0.15, { scale: 2, repeat: 1, yoyo: true }, '+=0.1');
                    tl.to(context.$('.text3-7'), 0.15, { scale: 2, repeat: 1, yoyo: true });
                };

                // 第四段文字
                function dialog4() {
                    var tl = new TimelineMax();
                    tl.to(context.$('.text4-1'), 0.15, { scale: 2, repeat: 1, yoyo: true });
                    tl.to(context.$('.text4-2'), 0.15, { scale: 2, repeat: 1, yoyo: true }, '-=0.1');
                    tl.to(context.$('.text4-3'), 0.15, { scale: 2, repeat: 1, yoyo: true }, '-=0.1');
                    tl.to(context.$('.text4-4'), 0.15, { scale: 2, repeat: 1, yoyo: true }, '-=0.1');
                };

                // 第五段文字
                function dialog5() {
                    var tl = new TimelineMax();
                    tl.to(context.$('.text5-1'), 0.15, { scale: 2, repeat: 1, yoyo: true });
                    tl.to(context.$('.text5-2'), 0.15, { scale: 2, repeat: 1, yoyo: true }, '-=0.1');
                    tl.to(context.$('.text5-3'), 0.15, { scale: 2, repeat: 1, yoyo: true }, '-=0.1');
                    tl.to(context.$('.text5-4'), 0.15, { scale: 2, repeat: 1, yoyo: true }, '+=1.3');
                    tl.to(context.$('.text5-5'), 0.15, { scale: 2, repeat: 1, yoyo: true }, '-=0.1');
                    tl.to(context.$('.text5-6'), 0.15, { scale: 2, repeat: 1, yoyo: true }, '-=0.1');
                    tl.to(context.$('.text5-7'), 0.15, { scale: 2, repeat: 1, yoyo: true }, '-=0.1');
                    tl.to(context.$('.text5-8'), 0.15, { scale: 2, repeat: 1, yoyo: true }, '-=0.1');
                    tl.to(context.$('.text5-9'), 0.15, { scale: 2, repeat: 1, yoyo: true }, '-=0.1');
                };

                // 第六段文字
                function dialog6() {
                    var tl = new TimelineMax();
                    tl.to(context.$('.text6-1'), 0.15, { scale: 2, repeat: 1, yoyo: true }, '+=0.1');
                    tl.to(context.$('.text6-2'), 0.15, { scale: 2, repeat: 1, yoyo: true }, '+=1.6');
                    tl.to(context.$('.text6-3'), 0.15, { scale: 2, repeat: 1, yoyo: true }, '-=0.1');
                    tl.to(context.$('.text6-4'), 0.15, { scale: 2, repeat: 1, yoyo: true }, '-=0.1');
                    tl.to(context.$('.text6-5'), 0.15, { scale: 2, repeat: 1, yoyo: true }, '-=0.1');
                };

                // 第七段文字
                function dialog7() {
                    var tl = new TimelineMax();
                    tl.to(context.$('.text7-1'), 0.15, { scale: 2, repeat: 1, yoyo: true });
                    tl.to(context.$('.text7-2'), 0.15, { scale: 2, repeat: 1, yoyo: true }, '-=0.1');
                    tl.to(context.$('.text7-3'), 0.15, { scale: 2, repeat: 1, yoyo: true }, '-=0.1');
                    tl.to(context.$('.text7-4'), 0.15, { scale: 2, repeat: 1, yoyo: true }, '-=0.1');
                    tl.to(context.$('.text7-5'), 0.15, { scale: 2, repeat: 1, yoyo: true }, '+=0.8');
                    tl.to(context.$('.text7-6'), 0.15, { scale: 2, repeat: 1, yoyo: true }, '-=0.1');
                    tl.to(context.$('.text7-7'), 0.2, { scale: 2, repeat: 1, yoyo: true }, '+=0.5');
                    tl.to(context.$('.text7-8'), 0.19, { scale: 2, repeat: 1, yoyo: true });
                };

                // 摆手
                function handShow(el, n) {
                    var tl = new TimelineMax();
                    tl.to(el, 0.15, { onComplete: function() { el.attr('src', 'assets/images/page6/hand' + n + '-2.png') } });
                    tl.to(el, 0.15, { onComplete: function() { el.attr('src', 'assets/images/page6/hand' + n + '-3.png') } });
                }

                // 狗子摆手
                function pawRubbing() {
                    var tl = new TimelineMax({ repeat: 26, repeatDelay: 0.1 }); // 26
                    tl.to(context.$('.paw'), 0.12, { onComplete: function() { context.$('.paw').attr('src', 'assets/images/page6/paw2.png') } });
                    tl.to(context.$('.paw'), 0.12, { onComplete: function() { context.$('.paw').attr('src', 'assets/images/page6/paw1.png') } });
                };

                // 狗子摇尾巴
                function tailShake(el, n) {
                    var tl = new TimelineMax({ repeat: n, repeatDelay: 0.1 }); // 14
                    tl.to(el, 0.16, { onComplete: function() { el.attr('src', 'assets/images/page6/tail2.png') } });
                    tl.to(el, 0.16, { onComplete: function() { el.attr('src', 'assets/images/page6/tail3.png') } });
                    tl.to(el, 0.16, { onComplete: function() { el.attr('src', 'assets/images/page6/tail1.png') } });
                };

                // 红心
                function heartTwinkle() {
                    var tl = new TimelineMax({ repeat: 20, repeatDelay: 0.1 }); // 20
                    tl.to(context.$('.heart'), 0.17, { onComplete: function() { context.$('.heart').attr('src', 'assets/images/page6/heart2.png') } });
                    tl.to(context.$('.heart'), 0.17, { onComplete: function() { context.$('.heart').attr('src', 'assets/images/page6/heart1.png') } });
                }

                // 流口水
                function drool() {
                    var tl = new TimelineMax({ repeat: 16, repeatDelay: 0.1 }); // 16
                    tl.to(context.$('.saliva'), 0.17, { onComplete: function() { context.$('.saliva').attr('src', 'assets/images/page6/saliva2.png') } });
                    tl.to(context.$('.saliva'), 0.17, { onComplete: function() { context.$('.saliva').attr('src', 'assets/images/page6/saliva3.png') } });
                    tl.to(context.$('.saliva'), 0.17, { onComplete: function() { context.$('.saliva').attr('src', 'assets/images/page6/saliva1.png') } });
                };

                // 蓝光圈
                function blueLight() {
                    var tl = new TimelineMax({ repeat: 9, repeatDelay: 0.1 }); // 16
                    tl.to(context.$('.dog2'), 0.17, { onComplete: function() { context.$('.dog2').attr('src', 'assets/images/page6/dog2.png') } });
                    tl.to(context.$('.dog2'), 0.17, { onComplete: function() { context.$('.dog2').attr('src', 'assets/images/page6/dog3.png') } });
                    tl.to(context.$('.dog2'), 0.17, { onComplete: function() { context.$('.dog2').attr('src', 'assets/images/page6/dog4.png') } });
                    tl.to(context.$('.dog2'), 0.17, { onComplete: function() { context.$('.dog2').attr('src', 'assets/images/page6/dog5.png') } });
                    tl.to(context.$('.dog2'), 0.17, { onComplete: function() { context.$('.dog2').attr('src', 'assets/images/page6/dog6.png') } });
                    tl.to(context.$('.dog2'), 0.17, { onComplete: function() { context.$('.dog2').attr('src', 'assets/images/page6/dog7.png') } });
                };

                // 眼睛闪烁
                function eyesTwinkle() {
                    var tl = new TimelineMax({ repeat: 26, repeatDelay: 0.1 }); // 16
                    tl.to(context.$('.eyes'), 0.17, { onComplete: function() { context.$('.eyes').attr('src', 'assets/images/page6/eye2.png') } });
                    tl.to(context.$('.eyes'), 0.17, { onComplete: function() { context.$('.eyes').attr('src', 'assets/images/page6/eye1.png') } });
                }

                // 跳舞
                function dance() {
                    var tl = new TimelineMax({ repeat: 7, repeatDelay: 0.1 }); // 16
                    tl.to(context.$('.dog3'), 0.17, { onComplete: function() { context.$('.dog3').attr('src', 'assets/images/page6/dog8.png') } });
                    tl.to(context.$('.dog3'), 0.17, { onComplete: function() { context.$('.dog3').attr('src', 'assets/images/page6/dog9.png') } });
                    tl.to(context.$('.dog3'), 0.17, { onComplete: function() { context.$('.dog3').attr('src', 'assets/images/page6/dog10.png') } });
                    tl.to(context.$('.dog3'), 0.17, { onComplete: function() { context.$('.dog3').attr('src', 'assets/images/page6/dog12.png') } });
                    tl.to(context.$('.dog3'), 0.17, { onComplete: function() { context.$('.dog3').attr('src', 'assets/images/page6/dog13.png') } });
                    tl.to(context.$('.dog3'), 0.17, { onComplete: function() { context.$('.dog3').attr('src', 'assets/images/page6/dog14.png') } });
                }

                // 播放音乐1
                function page6Play(name) {
                    console.log(name);
                    $('body').append('<audio id="' + name + '" src="assets/audio/page6/' + name + '.mp3" autoplay="" loop="false"></audio>');
                    var name = '#' + name;
                    if (window.WeixinJSBridge) {
                        WeixinJSBridge.invoke('getNetworkType', {}, function(e) {
                            $(name).get(0).play();
                            $(name).get(0).muted = app.isMute;
                        }, false);
                    } else {
                        document.addEventListener("WeixinJSBridgeReady", function() {
                            WeixinJSBridge.invoke('getNetworkType', {}, function(e) {
                                $(name).get(0).play();
                                $(name).get(0).muted = app.isMute;
                            });
                        }, false);
                    }
                    setTimeout(function() {
                        $(name).get(0).play();
                    }, 100);
                    $(name).get(0).muted = app.isMute;
                    $(name).get(0).oncanplay = function() {
                        var duration = $(name).get(0).duration;
                        current6Mp3 = $(name);
                        window.setTimeout(function() {
                            $(name).remove();
                        }, duration * 1000)
                    };
                };
            },
            afterRemove: function() {
                var context = this;
                if ($('audio').length > 0) {
                    for (var i = 0; i < $('audio').length; i++) {
                        $('audio')[i].pause();
                    }
                }
                // context.page1Play=function(){}
                context._t.stop();
            },
        });
        // Return the module for AMD compliance.
        return Page;
    })