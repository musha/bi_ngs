// Page module
define(["app", "controllers/base/page"],

    function(app, BasePage) {

        var Page = {};

        Page.View = BasePage.View.extend({
            fitOn: "width", //width, height, custom
            beforeRender: function() {
                var done = this.async();
                require(["vendor/greensock/utils/SplitText.min"],
                    function() {
                        done();
                    });
            },
            afterRender: function() {
                _mwt.push(['_trackPageview','/h5/6','06末页']);
                
                var context = this;

                // 主体动画
                var tl = new TimelineMax();
                context._t = tl;
                tl.from(context.$('.end'), 0.4, { autoAlpha: 0, onComplete: function() { dogEyesTwinkle() } }, 0.3);
                tl.to(context.$('.dog'), 0.1, { onComplete: function() {
                    endPlay('end1');
                    context.$('.dog').attr('src', 'assets/images/end/dog2.png');
                } }, '+=0.9');
                tl.to(context.$('.dog'), 0.1, { onComplete: function() {
                    context.$('.dog').attr('src', 'assets/images/end/dog3.png');
                } }, '+=0.9');
                tl.to(context.$('.dialog'), 0.1, { autoAlpha: 1 }, '+=0.8' );
                tl.to(context.$('.dialog'), 0.1, { autoAlpha: 0 }, '+=3.5');
                tl.to(context.$('.dog'), 0.1, { onComplete: function() {
                    context.$('.dog').attr('src', 'assets/images/end/dog4.png');
                } }, '+=0.5');
                tl.to(context.$('.scene1'), 0.2, { autoAlpha: 0 }, '+=1');
                tl.to(context.$('.scene2'), 0.2, { autoAlpha: 1 });
                tl.to(context.$('.photo'), 0.15, { onComplete: function() { context.$('.photo').attr('src', 'assets/images/end/photo2.png') } });
                tl.to(context.$('.photo'), 0.15, { onComplete: function() { context.$('.photo').attr('src', 'assets/images/end/photo3.png') } });
                tl.to(context.$('.button'), 0.2, { autoAlpha: 1 }, '+=0.5');

                var currentEndMp3;
                context.$(".button").click(function() {
                    if (currentEndMp3 != undefined) {
                        currentEndMp3.get(0).pause();
                    };
                    tl.kill();
                    app.router.goto("haibao");
                });

                // 狗眼闪烁
                function dogEyesTwinkle() {
                    var tl = new TimelineMax({ repeat: 6, yoyo: true });
                    tl.to(context.$('.eyes'), 0.12, { autoAlpha: 0 } );
                };

                // 播放音乐1
                function endPlay(name) {
                    console.log(name);
                    $('body').append('<audio id="' + name + '" src="assets/audio/end/' + name + '.mp3" autoplay="" loop="false"></audio>');
                    var name = '#' + name;
                    if (window.WeixinJSBridge) {
                        WeixinJSBridge.invoke('getNetworkType', {}, function(e) {
                            $(name).get(0).play();

                        }, false);
                    } else {
                        document.addEventListener("WeixinJSBridgeReady", function() {
                            WeixinJSBridge.invoke('getNetworkType', {}, function(e) {
                                $(name).get(0).play();

                            });
                        }, false);
                    }
                    setTimeout(function() {
                        $(name).get(0).play();
                    }, 100);

                    $(name).get(0).oncanplay = function() {
                        var duration = $(name).get(0).duration;
                        currentEndMp3 = $(name);
                        window.setTimeout(function() {
                            $(name).remove();
                        }, duration * 1000)
                    };
                };
            },
            afterRemove: function() {
                var context = this;
                if ($('audio').length > 0) {
                    for (var i = 0; i < $('audio').length; i++) {
                        $('audio')[i].pause();
                    }
                }
                // context.page1Play=function(){}
                context._t.stop();
            },
        });
        // Return the module for AMD compliance.
        return Page;
    })