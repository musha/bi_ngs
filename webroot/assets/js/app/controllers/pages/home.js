// Page module
define(["app", "controllers/base/page"],

    function(app, BasePage) {
        var Page = {};

        Page.View = BasePage.View.extend({
            fitOn: "width", //width, height, custom
            beforeRender: function() {
                var done = this.async();
                require(["vendor/zepto/zepto.html5Loader.min"],
                    function() {
                        done();
                    });
            },
            afterRender: function() {
                _mwt.push(['_trackPageview','/h5/1','01首页']);
                // 预加载
                var firstLoadFiles = {
                    "files": [{
                            "type": "AUDIO",
                            "sources": {
                                "mp3": {
                                    "source": "assets/audio/home/home_mic.mp3",
                                    "size": 1
                                },
                                "mp3": {
                                    "source": "assets/audio/page2/page2_mic1.mp3",
                                    "size": 1
                                },
                                "mp3": {
                                    "source": "assets/audio/page2/page2_mic2.mp3",
                                    "size": 1
                                },
                                "mp3": {
                                    "source": "assets/audio/page2/page2_mic3.mp3",
                                    "size": 1
                                },
                                "mp3": {
                                    "source": "assets/audio/page3/page3_mic1.mp3",
                                    "size": 1
                                },
                                "mp3": {
                                    "source": "assets/audio/page4/page4_mic.mp3",
                                    "size": 1
                                },
                                "mp3": {
                                    "source": "assets/audio/page5/page5_mic.mp3",
                                    "size": 1
                                },
                                "mp3": {
                                    "source": "assets/audio/page6/page6_mic1.mp3",
                                    "size": 1
                                },
                                "mp3": {
                                    "source": "assets/audio/page6/page6_mic2.mp3",
                                    "size": 1
                                },
                                "mp3": {
                                    "source": "assets/audio/page6/page6_mic3.mp3",
                                    "size": 1
                                },
                                "mp3": {
                                    "source": "assets/audio/end/end1.mp3",
                                    "size": 1
                                },
                                "mp4": {
                                    "source": "assets/audio/page1/timeTunnel.mp4",
                                    "size": 1
                                },
                                "mp4": {
                                    "source": "assets/audio/page1/episode1.mp4",
                                    "size": 1
                                },

                            },

                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/haibao/p1_new_all.png",
                            "size": 1
                        }, {
                            "type": "IMAGE",
                            "source": "assets/images/haibao/p1_new_ren.png",
                            "size": 1
                        }, {
                            "type": "IMAGE",
                            "source": "assets/images/haibao/p1_new.png",
                            "size": 1
                        }, {
                            "type": "IMAGE",
                            "source": "assets/images/haibao/p1.png",
                            "size": 1
                        }, {
                            "type": "IMAGE",
                            "source": "assets/images/haibao/p2.png",
                            "size": 1
                        }, {
                            "type": "IMAGE",
                            "source": "assets/images/haibao/p2_new_all.png",
                            "size": 1
                        }, {
                            "type": "IMAGE",
                            "source": "assets/images/haibao/p2_new_ren.png",
                            "size": 1
                        }, {
                            "type": "IMAGE",
                            "source": "assets/images/haibao/p2_new.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/haibao/p3.png",
                            "size": 1
                        }, {
                            "type": "IMAGE",
                            "source": "assets/images/haibao/p3_new_all.png",
                            "size": 1
                        }, {
                            "type": "IMAGE",
                            "source": "assets/images/haibao/p3_new_ren.png",
                            "size": 1
                        }, {
                            "type": "IMAGE",
                            "source": "assets/images/haibao/p3_new.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/haibao/p4.png",
                            "size": 1
                        }, {
                            "type": "IMAGE",
                            "source": "assets/images/haibao/p4_new_all.png",
                            "size": 1
                        }, {
                            "type": "IMAGE",
                            "source": "assets/images/haibao/p4_new_ren.png",
                            "size": 1
                        }, {
                            "type": "IMAGE",
                            "source": "assets/images/haibao/p4_new.png",
                            "size": 1
                        },
                    ]
                };
                $.html5Loader({
                    filesToLoad: firstLoadFiles,
                    onBeforeLoad: function() {},
                    onComplete: function() {

                    },
                    onElementLoaded: function(obj, elm) {},
                    onUpdate: function(percentage) {
                        console.log(percentage);
                    }
                });

                var context = this;

                // 主体动画
                var tl = new TimelineMax();
                tl.from(context.$('.home'), 0.4, {
                    autoAlpha: 0,
                    onComplete: function() {
                        pawShake();
                    }
                }, 0.3);
                tl.to(context.$('.dialog'), 0.8, {
                    autoAlpha: 1,
                    onStart: function() {
                        homePlay1('home_mic');
                    },
                    onComplete: function() {
                        setTimeout(function() {
                            tl.kill();
                            app.router.goto('page1');
                        }, 4500)
                    },
                }, '+=0.4');

                // 狗子挥爪
                function pawShake() {
                    var tl = new TimelineMax();
                    tl.to(context.$('.dog'), 0.2, { onComplete: function() { context.$('.dog').attr('src', 'assets/images/home/dog2.png') } });
                    tl.to(context.$('.dog'), 0.2, { onComplete: function() { context.$('.dog').attr('src', 'assets/images/home/dog3.png') } });
                    tl.to(context.$('.dog'), 0.2, { onComplete: function() { context.$('.dog').attr('src', 'assets/images/home/dog1.png') } }, 0.8);
                    tl.to(context.$('.dog'), 0.2, { onComplete: function() { context.$('.dog').attr('src', 'assets/images/home/dog2.png') } });
                    tl.to(context.$('.dog'), 0.2, { onComplete: function() { context.$('.dog').attr('src', 'assets/images/home/dog3.png') } });
                    tl.to(context.$('.dog'), 0.2, { onComplete: function() { context.$('.dog').attr('src', 'assets/images/home/dog1.png') } }, 2);
                    tl.to(context.$('.dog'), 0.2, { onComplete: function() { context.$('.dog').attr('src', 'assets/images/home/dog2.png') } });
                    tl.to(context.$('.dog'), 0.2, { onComplete: function() { context.$('.dog').attr('src', 'assets/images/home/dog3.png') } });
                };

                // 播放音乐
                function homePlay1(name) {
                    console.log(name);
                    $('body').append('<audio id="' + name + '" src="assets/audio/home/' + name + '.mp3" autoplay="" loop="false"></audio>');
                    var name = '#' + name;
                    if (window.WeixinJSBridge) {
                        WeixinJSBridge.invoke('getNetworkType', {}, function(e) {
                            $(name).get(0).play();

                        }, false);
                    } else {
                        document.addEventListener("WeixinJSBridgeReady", function() {
                            WeixinJSBridge.invoke('getNetworkType', {}, function(e) {
                                $(name).get(0).play();

                            });
                        }, false);
                    }
                    setTimeout(function() {
                        $(name).get(0).play();
                    }, 100);
                    $(name).get(0).oncanplay = function() {
                        var duration = $(name).get(0).duration;
                        window.setTimeout(function() {
                            $(name).remove();
                        }, duration * 1000)
                    };
                }
            },
        });
        // Return the module for AMD compliance.
        return Page;
    })