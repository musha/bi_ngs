// Page module
define(["app", "controllers/base/page"],

    function(app, BasePage) {
        var Page = {};

        Page.View = BasePage.View.extend({
            fitOn: "width", //width, height, custom
            beforeRender: function() {
                var done = this.async();
                require(["vendor/greensock/utils/SplitText.min"],
                    function() {
                        done();
                    });
            },
            afterRender: function() {
                var context = this;
                _mwt.push(['_trackPageview', '/h5/page1', 'page1']);
                // 主体动画
                $('.skip').css('top', $(window).height() * 0.9);
                app.skip_arrow();
                var tl = new TimelineMax();
                context._t = tl;
                tl.from(context.$('.page1'), 0.4, { autoAlpha: 0, onComplete: function() {} }, 0.3);

                // 播放视频
                // 监测是否是安卓
                if ($.browser.android) {
                    context.$(".poster").show();
                    context.$(".poster").click(function() {
                        context.$(".poster").hide();
                        context.$("#video1").show();
                        document.getElementById("video1").play();
                    });
                    // 不是安卓
                } else {
                    var video = context.$("#video1").get(0);
                    if (window.WeixinJSBridge) {
                        WeixinJSBridge.invoke('getNetworkType', {}, function(e) {
                            video.play();
                        }, false);
                    } else {
                        document.addEventListener("WeixinJSBridgeReady", function() {
                            WeixinJSBridge.invoke('getNetworkType', {}, function(e) {
                                video.play();
                            });
                        }, false);
                    }
                    video.play();
                };

                var md = document.getElementById("video1");
                if (md.ended) {
                    console.log("结束");
                };
                md.addEventListener("ended", function() {
                    console.log("结束");
                    context.$(".poster").hide();
                    context.$("#video1").hide();
                    context.$(".scene").show();
                });

                // 打开视频
                $('.play').on('click', function() {
                    $('.modal').show();
                });

                // 关闭视频 
                $('.close').on('click', function() {
                    var video = context.$("#video2").get(0);
                    video.pause();
                    $('.modal').hide();
                });

                // skip
                var current1Mp3;
                $('.skip').on('click', function() {
                    if (current1Mp3 != undefined) {
                        current1Mp3.get(0).pause();
                    };
                    tl.kill();
                    app.router.goto('page2');
                });

                // 播放音乐1
                function page1Play(name) {
                    console.log(name);
                    $('body').append('<audio id="' + name + '" src="assets/audio/page1/' + name + '.mp3" autoplay="" loop="false"></audio>');
                    var name = '#' + name;
                    if (window.WeixinJSBridge) {
                        WeixinJSBridge.invoke('getNetworkType', {}, function(e) {
                            $(name).get(0).play();

                        }, false);
                    } else {
                        document.addEventListener("WeixinJSBridgeReady", function() {
                            WeixinJSBridge.invoke('getNetworkType', {}, function(e) {
                                $(name).get(0).play();

                            });
                        }, false);
                    }
                    setTimeout(function() {
                        $(name).get(0).play();
                    }, 100);
                    $(name).get(0).oncanplay = function() {
                        var duration = $(name).get(0).duration;
                        current1Mp3 = $(name);
                        window.setTimeout(function() {
                            $(name).remove();
                        }, duration * 1000)
                    };

                };

                resize();

                function resize() {

                    var ww = $(window).width();
                    var wh = $(window).height();
                    setInterval(function() {
                        if (wh / ww < 1.5) {
                            // context.$('.ratio-fix').css('padding-bottom', '148%');
                        }
                        if (1.5 < wh / ww < 1.51) {
                            // context.$('.ratio-fix').css('padding-bottom', '151%');
                        }
                    }, 500)
                }
            },
            afterRemove: function() {
                var context = this;
                if ($('audio').length > 0) {
                    for (var i = 0; i < $('audio').length; i++) {
                        $('audio')[i].pause();
                    }
                }
                // context.page1Play=function(){}
                context._t.stop();
            },
        });
        // Return the module for AMD compliance.
        return Page;
    })