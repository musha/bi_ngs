// Page module
define(["app", "controllers/base/page"],

    function(app, BasePage) {

        var Page = {};

        Page.View = BasePage.View.extend({

            fitOn: "width", //width, height, custom

            beforeRender: function() {
                var done = this.async();
                require(["vendor/greensock/utils/SplitText.min"],
                    function() {
                        done();
                    });
            },

            afterRender: function() {
                var context = this;
                _mwt.push(['_trackPageview', '/h5/page3', 'page3']);
                // 主体动画
                $('.skip').css('top', $(window).height() * 0.9);
                app.skip_arrow();
                var tl = new TimelineMax();
                context._t = tl;
                tl.from(context.$('.page3'), 0, {
                    autoAlpha: 0,
                    onComplete: function() {
                        page3Play('pg3_mic1');
                        lightTwinkle(context.$('.girl-lg'), '');
                    }
                });
                tl.to(context.$('.scene1'), 0, { autoAlpha: 0 }, '+=2.5');
                tl.to(context.$('.scene2'), 0, {
                    autoAlpha: 1,
                    onComplete: function() {
                        page3Play('pg3_mic1');
                        lightTwinkle(context.$('.girl'), 's');
                    }
                });
                tl.to(context.$('.page3'), 0.1, {
                    onComplete: function() {
                        tl.kill();
                        app.router.goto('page4');
                    }
                }, '+=3');

                // skip
                var current3Mp3;
                $('.skip').on('click', function() {
                    if (current3Mp3 != undefined) {
                        current3Mp3.get(0).pause();
                    }
                    tl.kill();
                    app.router.goto('page4');
                });

                // 光芒
                function lightTwinkle(el, size) {
                    var tl = new TimelineMax({ repeat: -1 });
                    tl.to(el, 0.19, { onComplete: function() { el.attr('src', 'assets/images/page3/light' + size + '2.png') } });
                    tl.to(el, 0.19, { onComplete: function() { el.attr('src', 'assets/images/page3/light' + size + '3.png') } });
                    tl.to(el, 0.19, { onComplete: function() { el.attr('src', 'assets/images/page3/light' + size + '4.png') } });
                    tl.to(el, 0.19, { onComplete: function() { el.attr('src', 'assets/images/page3/light' + size + '1.png') } });
                    tl.to(el, 0.19, { onComplete: function() { el.attr('src', 'assets/images/page3/light' + size + '1.png') } });
                };

                // 播放音乐
                function page3Play(name) {
                    console.log(name);
                    $('body').append('<audio id="' + name + '" src="assets/audio/page3/' + name + '.mp3" autoplay="" loop="false"></audio>');
                    var name = '#' + name;
                    if (window.WeixinJSBridge) {
                        WeixinJSBridge.invoke('getNetworkType', {}, function(e) {
                            $(name).get(0).play();
                            $(name).get(0).muted = app.isMute;
                        }, false);
                    } else {
                        document.addEventListener("WeixinJSBridgeReady", function() {
                            WeixinJSBridge.invoke('getNetworkType', {}, function(e) {
                                $(name).get(0).play();
                                $(name).get(0).muted = app.isMute;
                            });
                        }, false);
                    }
                    setTimeout(function() {
                        $(name).get(0).play();
                    }, 100);
                    $(name).get(0).muted = app.isMute;
                    $(name).get(0).oncanplay = function() {
                        var duration = $(name).get(0).duration;
                        current3Mp3 = $(name);
                        window.setTimeout(function() {
                            $(name).remove();
                        }, duration * 1000)
                    };

                };

                resize();

                function resize() {

                    var ww = $(window).width();
                    var wh = $(window).height();
                    setInterval(function() {
                        if (wh / ww < 1.5) {
                            context.$('.text_2').css('bottom', '12%');
                        }
                        if (1.5 < wh / ww < 1.51) {
                            context.$('.text_2').css('bottom', '12%');
                        }
                    }, 500)
                }
            },
            afterRemove: function() {
                var context = this;
                if ($('audio').length > 0) {
                    for (var i = 0; i < $('audio').length; i++) {
                        $('audio')[i].pause();
                    }
                }
                // context.page1Play=function(){}
                context._t.stop();
            },
        });
        // Return the module for AMD compliance.
        return Page;
    })