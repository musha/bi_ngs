// Page module
define(["app", "controllers/base/page"],

    function(app, BasePage) {
        var Page = {};
        Page.View = BasePage.View.extend({
            fitOn: "width", //width, height, custom
            beforeRender: function() {
                var done = this.async();
                require(["vendor/greensock/utils/SplitText.min"],
                    function() {
                        done();
                    });
            },

            afterRender: function() {
                var context = this;
                _mwt.push(['_trackPageview', '/h5/page2', 'page2']);
                // 主体动画
                $('.skip').css('top', $(window).height() * 0.9);
                app.skip_arrow();
                var tl = new TimelineMax();
                context._t = tl;
                tl.from(context.$('.page2'), 0.4, {
                    autoAlpha: 0,
                    onComplete: function() {
                        page2Play('pg2_mic1');
                        doorOpen();
                    }
                }, 0.3);
                tl.to(context.$('.bg-dog'), 0.2, {
                    onComplete: function() {
                        context.$('.bg-dog').attr('src', 'assets/images/page2/dog2.png');
                    }
                }, '+=0.2');
                tl.to(context.$('.thinking'), 0.2, {
                    autoAlpha: 1,
                    onStart: function() {
                        page2Play('pg2_mic2');
                        turnHead();
                    }
                }, '+=0.5');
                tl.to(context.$('.dialog'), 0.1, { autoAlpha: 1 }, '+=1.5');
                tl.to(context.$('.dialog'), 0.1, {
                    onComplete: function() {
                        page2Play('pg2_mic3');
                    }
                }, '+=8.5');
                tl.to(context.$('.dialog'), 0.1, {
                    onComplete: function() {
                        context.$('.dialog').attr('src', 'assets/images/page2/dialog2.png');
                    }
                }, '+=1.5');
                tl.to(context.$('.dog'), 0.1, {
                    onComplete: function() {
                        context.$('.dog').attr('src', 'assets/images/page2/dog-smile1.png');
                        smell('smile', -1);
                    }
                }, '+=2.5');
                tl.to(context.$('.close-up'), 0.1, {
                    display: 'block',
                    onComplete: function() {
                        closeUp();
                    }
                }, '+=3');
                tl.to(context.$('.page2'), 0.1, {
                    onComplete: function() {
                        tl.kill();
                        app.router.goto('page3');
                    }
                }, '+=2');

                // skip
                var current2Mp3;
                $('.skip').on('click', function() {
                    if (current2Mp3 != undefined) {
                        current2Mp3.get(0).pause();
                    };
                    tl.kill();
                    app.router.goto('page3');
                });

                // 嗅气味
                function smell(dir, n) {
                    var tl = new TimelineMax({ repeat: n });
                    tl.to(context.$('.dog'), 0.17, { onComplete: function() { context.$('.dog').attr('src', 'assets/images/page2/dog-' + dir + '2.png') } });
                    tl.to(context.$('.dog'), 0.17, { onComplete: function() { context.$('.dog').attr('src', 'assets/images/page2/dog-' + dir + '1.png') } });
                };

                // 开门
                function doorOpen() {
                    var tl = new TimelineMax();
                    tl.to(context.$('.door'), 0.1, { onComplete: function() { context.$('.door').attr('src', 'assets/images/page2/door2.png') } });
                    tl.to(context.$('.door'), 0.1, { onComplete: function() { context.$('.door').attr('src', 'assets/images/page2/door3.png') } });
                    tl.to(context.$('.door'), 0.1, { display: 'none', onComplete: function() { context.$('.bg').attr('src', 'assets/images/page2/bg2.png') } });
                };

                // 狗子转头
                function turnHead() {
                    var tl = new TimelineMax({});
                    tl.to(context.$('.dog'), 0.1, { onComplete: function() { context.$('.dog').attr('src', 'assets/images/page2/dog-left1.png');
                            smell('left', 3); } });
                    tl.to(context.$('.dog'), 0.1, { onComplete: function() { context.$('.dog').attr('src', 'assets/images/page2/dog-center1.png');
                            smell('center', 3); } }, '+=2');
                    tl.to(context.$('.dog'), 0.1, { onComplete: function() { context.$('.dog').attr('src', 'assets/images/page2/dog-right1.png');
                            smell('right', 3); } }, '+=2');
                    tl.to(context.$('.dog'), 0.1, { onComplete: function() { context.$('.dog').attr('src', 'assets/images/page2/dog-left1.png');
                            smell('left', 3); } }, '+=2');
                    tl.to(context.$('.dog'), 0.1, { onComplete: function() { context.$('.dog').attr('src', 'assets/images/page2/dog-center1.png');
                            smell('center', 3); } }, '+=2');
                    tl.to(context.$('.dog'), 0.1, { onComplete: function() { context.$('.dog').attr('src', 'assets/images/page2/dog-right1.png');
                            smell('right', 9); } }, '+=2');
                };

                // 特写
                function closeUp() {
                    var tl = new TimelineMax();
                    tl.to(context.$('.close-up'), 0.3, { onComplete: function() { context.$('.close-up').attr('src', 'assets/images/page2/close-up2.png') } });
                    tl.to(context.$('.close-up'), 0.3, { onComplete: function() { context.$('.close-up').attr('src', 'assets/images/page2/close-up3.png') } });
                    tl.to(context.$('.close-up'), 0.3, { onComplete: function() { context.$('.close-up').attr('src', 'assets/images/page2/close-up4.png') } });
                };

                // 播放音乐1
                function page2Play(name) {
                    console.log(name);
                    $('body').append('<audio id="' + name + '" src="assets/audio/page2/' + name + '.mp3" autoplay="" loop="false"></audio>');
                    var name = '#' + name;
                    if (window.WeixinJSBridge) {
                        WeixinJSBridge.invoke('getNetworkType', {}, function(e) {
                            $(name).get(0).play();
                            $(name).get(0).muted = app.isMute;
                        }, false);
                    } else {
                        document.addEventListener("WeixinJSBridgeReady", function() {
                            WeixinJSBridge.invoke('getNetworkType', {}, function(e) {
                                $(name).get(0).play();
                                $(name).get(0).muted = app.isMute;
                            });
                        }, false);
                    }
                    setTimeout(function() {
                        $(name).get(0).play();
                    }, 100);
                    $(name).get(0).muted = app.isMute;
                    $(name).get(0).oncanplay = function() {
                        var duration = $(name).get(0).duration;
                        current2Mp3 = $(name);
                        window.setTimeout(function() {
                            $(name).remove();
                        }, duration * 1000)
                    };
                };

                resize();

                function resize() {
                    var ww = $(window).width();
                    var wh = $(window).height();
                    setInterval(function() {
                        if (wh / ww < 1.5) {
                            context.$('.ratio-fix').css('padding-bottom', '157%');
                        }
                    }, 500)
                }
            },
            afterRemove: function() {
                var context = this;
                if ($('audio').length > 0) {
                    for (var i = 0; i < $('audio').length; i++) {
                        $('audio')[i].pause();
                    }
                }
                // context.page1Play=function(){}
                context._t.stop();
            },
        });
        // Return the module for AMD compliance.
        return Page;
    })