// Page module
define(["app", "controllers/base/page"],

    function(app, BasePage) {

        var Page = {};

        Page.View = BasePage.View.extend({
            fitOn: "width", //width, height, custom
            beforeRender: function() {
                var done = this.async();
                require(["vendor/greensock/utils/SplitText.min"],
                    function() {
                        done();
                    });
            },
            afterRender: function() {
                var context = this;
                _mwt.push(['_trackPageview', '/h5/page4', 'page4']);
                // 主体动画
                $('.skip').css('top', $(window).height() * 0.9);
                app.skip_arrow();
                var tl = new TimelineMax();
                context._t = tl;
                tl.from(context.$('.page4'), 0.4, { autoAlpha: 0, onComplete: function() { page4Play('pg4_mic');
                        pawBeg();
                        dogEyes() } }, 0.3);
                tl.to(context.$('.dialog'), 0.1, { autoAlpha: 1 }, '+=2.5');
                tl.to(context.$('.page4'), 0.1, {
                    onComplete: function() {
                        tl.kill();
                        app.router.goto('page5');
                    }
                }, '+=4.5');

                // skip
                var current4Mp3;
                $('.skip').on('click', function() {
                    if (current4Mp3 != undefined) {
                        current4Mp3.get(0).pause();
                    }
                    tl.kill();
                    app.router.goto('page5');
                });

                // 眼睛闪烁 
                function dogEyes() {
                    var tl = new TimelineMax({ repeat: -1 });
                    tl.to(context.$('.dog'), 0.15, { onComplete: function() { context.$('.dog').attr('src', 'assets/images/page4/dog2.png') } });
                    tl.to(context.$('.dog'), 0.15, { onComplete: function() { context.$('.dog').attr('src', 'assets/images/page4/dog1.png') } });
                }

                // 狗爪动画
                function pawBeg() {
                    var tl = new TimelineMax({ repeat: -1 });
                    tl.to(context.$('.paw'), 0.15, { onComplete: function() { context.$('.paw').attr('src', 'assets/images/page4/paw2.png') } });
                    tl.to(context.$('.paw'), 0.15, { onComplete: function() { context.$('.paw').attr('src', 'assets/images/page4/paw1.png') } });
                };

                // 播放音乐1
                function page4Play(name) {
                    console.log(name);
                    $('body').append('<audio id="' + name + '" src="assets/audio/page4/' + name + '.mp3" autoplay="" loop="false"></audio>');
                    var name = '#' + name;
                    if (window.WeixinJSBridge) {
                        WeixinJSBridge.invoke('getNetworkType', {}, function(e) {
                            $(name).get(0).play();
                            $(name).get(0).muted = app.isMute;
                        }, false);
                    } else {
                        document.addEventListener("WeixinJSBridgeReady", function() {
                            WeixinJSBridge.invoke('getNetworkType', {}, function(e) {
                                $(name).get(0).play();
                                $(name).get(0).muted = app.isMute;
                            });
                        }, false);
                    }
                    setTimeout(function() {
                        $(name).get(0).play();
                    }, 100);
                    $(name).get(0).muted = app.isMute;
                    $(name).get(0).oncanplay = function() {
                        var duration = $(name).get(0).duration;
                        current4Mp3 = $(name);
                        window.setTimeout(function() {
                            $(name).remove();
                        }, duration * 1000)
                    };

                };

                resize();

                function resize() {
                    var ww = $(window).width();
                    var wh = $(window).height();
                    setInterval(function() {
                        if (wh / ww < 1.5) {
                            context.$('.text_3').css('top', '74%');
                        }
                        if (1.5 < wh / ww < 1.51) {
                            context.$('.text_3').css('top', '75%');
                        }
                    }, 500)
                }
            },
            afterRemove: function() {
                var context = this;
                if ($('audio').length > 0) {
                    for (var i = 0; i < $('audio').length; i++) {
                        $('audio')[i].pause();
                    }
                }
                // context.page1Play=function(){}
                context._t.stop();
            },
        });
        // Return the module for AMD compliance.
        return Page;
    })