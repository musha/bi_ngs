// Page module
define(["app", "controllers/base/page"],

    function(app, BasePage) {

        var Page = {};

        Page.View = BasePage.View.extend({
            fitOn: "width", //width, height, custom
            beforeRender: function() {
                var done = this.async();
                require(["vendor/greensock/utils/SplitText.min"],
                    function() {
                        done();
                    });
            },
            afterRender: function() {
                var context = this;
                _mwt.push(['_trackPageview', '/h5/page5', 'page5']);
                // 主体动画
                $('.skip').css('top', $(window).height() * 0.9);
                app.skip_arrow();
                var tl = new TimelineMax();
                context._t = tl;
                tl.from(context.$('.page5'), 0.4, {
                    autoAlpha: 0,
                    onComplete: function() {
                        page5Play('pg5_mic');
                        speak();
                        handShake();
                    }
                }, 0.3);
                tl.to(context.$('.dialog'), 0.1, { autoAlpha: 1 }, '+=1.2');
                tl.to(context.$('.dialog'), 0.1, {
                    onComplete: function() {
                        context.$('.dialog').attr('src', 'assets/images/page5/dialog2.png');
                    }
                }, '+=3');
                tl.to(context.$('.girl'), 0.5, {
                    onComplete: function() {
                        context.$('.girl').attr('src', 'assets/images/page5/girl3.png');
                    }
                }, '+=3.5');
                tl.to(context.$('.dialog'), 0.1, { autoAlpha: 0 });
                tl.to(context.$('.page5'), 0.1, {
                    onComplete: function() {
                        tl.kill();
                        app.router.goto('page6');
                    }
                }, '+=1');

                // skip
                var current5Mp3;
                $('.skip').on('click', function() {
                    if (current5Mp3 != undefined) {
                        current5Mp3.get(0).pause();
                    }
                    tl.kill();
                    app.router.goto('page6');
                });

                // 说话
                function speak() {
                    var tl = new TimelineMax({ repeat: 18 });
                    tl.to(context.$('.girl'), 0.2, { onComplete: function() { context.$('.girl').attr('src', 'assets/images/page5/girl2.png') } });
                    tl.to(context.$('.girl'), 0.2, { onComplete: function() { context.$('.girl').attr('src', 'assets/images/page5/girl1.png') } });
                };

                // 摆手
                function handShake() {
                    var tl = new TimelineMax({ repeat: 8, repeatDelay: 0.2 });
                    tl.to(context.$('.hand'), 0.23, { onComplete: function() { context.$('.hand').attr('src', 'assets/images/page5/hand2.png') } });
                    tl.to(context.$('.hand'), 0.23, { onComplete: function() { context.$('.hand').attr('src', 'assets/images/page5/hand3.png') } });
                    tl.to(context.$('.hand'), 0.23, { onComplete: function() { context.$('.hand').attr('src', 'assets/images/page5/hand1.png') } });
                };

                // 播放音乐1
                function page5Play(name) {
                    console.log(name);
                    $('body').append('<audio id="' + name + '" src="assets/audio/page5/' + name + '.mp3" autoplay="" loop="false"></audio>');
                    var name = '#' + name;
                    if (window.WeixinJSBridge) {
                        WeixinJSBridge.invoke('getNetworkType', {}, function(e) {
                            $(name).get(0).play();
                            $(name).get(0).muted = app.isMute;
                        }, false);
                    } else {
                        document.addEventListener("WeixinJSBridgeReady", function() {
                            WeixinJSBridge.invoke('getNetworkType', {}, function(e) {
                                $(name).get(0).play();
                                $(name).get(0).muted = app.isMute;
                            });
                        }, false);
                    }
                    setTimeout(function() {
                        $(name).get(0).play();
                    }, 100);
                    $(name).get(0).muted = app.isMute;
                    $(name).get(0).oncanplay = function() {
                        var duration = $(name).get(0).duration;
                        current5Mp3 = $(name);
                        window.setTimeout(function() {
                            $(name).remove();
                        }, duration * 1000)
                    };

                };
            },
            afterRemove: function() {
                var context = this;
                if ($('audio').length > 0) {
                    for (var i = 0; i < $('audio').length; i++) {
                        $('audio')[i].pause();
                    }
                }
                // context.page1Play=function(){}
                context._t.stop();
            },
        });
        // Return the module for AMD compliance.
        return Page;
    })