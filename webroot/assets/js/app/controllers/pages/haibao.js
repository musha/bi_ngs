// Page module
define(["app", "controllers/base/page"],

    function(app, BasePage) {

        var Page = {};

        Page.View = BasePage.View.extend({

            fitOn: "width", //width, height, custom
            beforeRender: function() {

                var done = this.async();

                require(["vendor/greensock/utils/SplitText.min"],
                    function() {
                        done();
                    });


            },
            beforeRender: function() {
                $('#bgMusic').remove();
            },
            afterRender: function() {

                app.page1url = '';
                app.page2url = '';
                var context = this;
                var tl = new TimelineMax({ repeat: 2, yoyo: true });
                tl.to($('.swip'), 0.6, { autoAlpha: 0 }, "+=0.6");

                setTimeout(function() {
                    app.mySwiper = new Swiper(this.$('.dosing_swiper'), {
                        slidesPerView: 1,
                        loop: true,
                        navigation: {
                            nextEl: '.swiper-button-next',
                            prevEl: '.swiper-button-prev',
                        }
                        // ,
                        // pagination: {
                        //     el: '.swiper-pagination',
                        //     type: 'bullets',
                        // },
                    });
                    $(".bt").click(function() {
                        app.router.goto("yinpian1");
                        if (app.mySwiper.activeIndex == 1) {
                            app.haibaoIndex = "1";
                            gtag('event', '海报1', {
                                'event_category': 'haibao1',
                                'event_label': 'click'
                            });
                            // _mwt.push(['_trackEvent', '海报1', 'haibao1']);
                        }
                        if (app.mySwiper.activeIndex == 2) {
                            app.haibaoIndex = "2";
                            gtag('event', '海报2', {
                                'event_category': 'haibao2',
                                'event_label': 'click'
                            });
                            // _mwt.push(['_trackEvent', '海报2', 'haibao2']);
                        }
                        if (app.mySwiper.activeIndex == 3) {
                            app.haibaoIndex = "3";
                            gtag('event', '海报3', {
                                'event_category': 'haibao3',
                                'event_label': 'click'
                            });
                            // _mwt.push(['_trackEvent', '海报3', 'haibao3']);
                        }
                        if (app.mySwiper.activeIndex == 4 || app.mySwiper.activeIndex == 0) {
                            app.haibaoIndex = "4";
                            gtag('event', '海报4', {
                                'event_category': 'haibao4',
                                'event_label': 'click'
                            });
                            // _mwt.push(['_trackEvent', '海报4', 'haibao4']);
                        }
                    })
                    $(".swiper-slide").on("click", function() {});
                    app.showtext = true;



                }, 800)
            },
            beforeRemove: function() {
                //this.mySwiper = null; 
                //this.$('.dosing_swiper').remove(); 
            }
        });

        // Return the module for AMD compliance.
        return Page;

    });