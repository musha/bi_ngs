define([

        "controllers/pages/ren",
        "controllers/pages/gou",
        "controllers/pages/haibao",
        "controllers/pages/yinpian1",
        "controllers/pages/yinpian2",

        "controllers/pages/end",
        "controllers/pages/home",
        "controllers/pages/page1",
        "controllers/pages/page2",
        "controllers/pages/page3",
        "controllers/pages/page4",
        "controllers/pages/page5",
        "controllers/pages/page6",
    ],


    function(Ren, Gou, Haibao, Yinpian1, Yinpian2, End, Home, Page1, Page2, Page3, Page4, Page5, Page6) {

        var pages = [

            {
                routeId: 'ren',
                type: 'main',
                landing: false,
                page: function() {
                    return new Ren.View({ template: "pages/ren" });
                }
            },
            {
                routeId: 'gou',
                type: 'main',
                landing: false,
                page: function() {
                    return new Gou.View({ template: "pages/gou" });
                }
            },
            {
                routeId: 'haibao',
                type: 'main',
                landing: false,
                page: function() {
                    return new Haibao.View({ template: "pages/haibao" });
                }
            },
            {
                routeId: 'yinpian1',
                type: 'main',
                landing: false,
                page: function() {
                    return new Yinpian1.View({ template: "pages/yinpian1" });
                }
            },
            {
                routeId: 'yinpian2',
                type: 'main',
                landing: false,
                page: function() {
                    return new Yinpian2.View({ template: "pages/yinpian2" });
                }
            },
            {
                routeId: 'end',
                type: 'main',
                landing: false,
                page: function() {
                    return new End.View({ template: "pages/end" });
                }
            },
            {
                routeId: 'home',
                type: 'main',
                landing: true,
                page: function() {
                    return new Home.View({ template: "pages/home" });
                }
            },
            {
                routeId: 'page1',
                type: 'main',
                landing: false,
                page: function() {
                    return new Page1.View({ template: "pages/page1" });
                }
            },
            {
                routeId: 'page2',
                type: 'main',
                landing: false,
                page: function() {
                    return new Page2.View({ template: "pages/page2" });
                }
            },
            {
                routeId: 'page3',
                type: 'main',
                landing: false,
                page: function() {
                    return new Page3.View({ template: "pages/page3" });
                }
            },
            {
                routeId: 'page4',
                type: 'main',
                landing: false,
                page: function() {
                    return new Page4.View({ template: "pages/page4" });
                }
            },
            {
                routeId: 'page5',
                type: 'main',
                landing: false,
                page: function() {
                    return new Page5.View({ template: "pages/page5" });
                }
            },
            {
                routeId: 'page6',
                type: 'main',
                landing: false,
                page: function() {
                    return new Page6.View({ template: "pages/page6" });
                }
            }

        ];


        return pages;

    });