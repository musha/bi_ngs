require(["app", "managers/layout-manager", "managers/router", "managers/resize-manager", "managers/tracker", "managers/ui-framework7"],


    function(app, LayoutManager, Router, ResizeManager, Tracker, Framework7) {

        // layout manager
        app.layout = (new LayoutManager()).layout();

        // router
        app.router = new Router();

        app.resizeManager = new ResizeManager();

        // app.framework7 = new Framework7();

        app.tracker = new Tracker({ gaAccount: "UA-125062866-1", baiduAccount: "" });

        app.eventBus = _.extend({}, Backbone.Events);


        /*
        app.yaogun = new Howl({
            src: [app.assetsPath + 'audio/yaogun.mp3'],
            loop: false,
            volume: 1,
            sprite: {
                complete: [0, 14000],

            },

        });

        app.yindu = new Howl({
            src: [app.assetsPath + 'audio/yindu.mp3'],
            loop: false,
            volume: 1,
            sprite: {
                complete: [0, 10000],

            },

        });

        app.xiha = new Howl({
            src: [app.assetsPath + 'audio/xiha.mp3'],
            loop: false,
            volume: 1,
            sprite: {
                complete: [0, 11000],

            },

        });
        */

        app.isSoundPlaying = true;
        /*
         app.user = new User.Model();
         app.user.getInfo();
         */
        // app.audio = $(".homeAudio").get(0);


        app.layout.render().promise().done(function() {
            Backbone.history.start({
                pushState: false
            });
        });
        var shareUrlFd = "http://campaign.archisense.cn/bi_ngs2/" + "?utm_source=wechat_share&utm_medium=share_fd&utm_campaign=teaser";

        var shareUrlTl = "http://campaign.archisense.cn/bi_ngs2/" + "?utm_source=wechat_share&utm_medium=share_tl&utm_campaign=teaser";

        app.shareDataFriend = {
            title: '亲密关系惊现神转机',
            desc: '这个OK很OK',
            link: shareUrlFd,
            imgUrl: "http://" + window.location.host + "/bi_ngs2/assets/images/share.jpg",
            success: function() {
                app.tracker.event('shared', 'event', 'sharefd');

            }
        };
        app.shareDataTimeLine = {
            title: '亲密关系惊现神转机',
            desc: '这个OK很OK',
            link: shareUrlTl,
            imgUrl: "http://" + window.location.host + "/bi_ngs2/assets/images/share.jpg",
            success: function() {
                app.tracker.event('shared', 'event', 'sharetl');
            }
        };







        if (wxr_openid == "") {
            wxr_openid = "test";
        }

        if (wxr_hash == "") {
            wxr_hash = "de34084aefb688a16f629b493f98342d";
        }



        if (window.location.host.indexOf("campaign.archisense.cn") != -1) {

            require(["//res.wx.qq.com/open/js/jweixin-1.0.0.js"],
                function(wx) {

                    app.wx = window.wx;

                    $.ajax({
                        url: '/wechat/api/share.php',
                        data: { url: document.location.href.split("#")[0] },
                        cache: false,
                        success: function(data) {

                            app.wx.config({
                                "debug": false,
                                "appId": data.appId,
                                "timestamp": data.timestamp,
                                "nonceStr": data.nonceStr,
                                "signature": data.signature,
                                "jsApiList": ["onMenuShareTimeline", "onMenuShareAppMessage", "onMenuShareQQ", "onMenuShareWeibo", "onMenuShareQZone"]
                            });

                            app.wx.ready(function() {

                                app.wx.onMenuShareAppMessage(app.shareDataFriend);
                                app.wx.onMenuShareTimeline(app.shareDataTimeLine);
                            });

                        },
                        error: function(XHR, textStatus, errorThrown) {

                        }
                    });


                });


        }


    });