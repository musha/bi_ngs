<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;

use app\actions\CaptchaAction;

use app\models\Comment;

use yii\db\Expression;



class SiteController extends Controller
{

    public $enableCsrfValidation = false;

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    


    public function actionSubmitComment()
    {

        header('Access-Control-Allow-Origin: *');

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;



        $openid = Yii::$app->request->post("openid");

        $nickname = Yii::$app->request->post("nickname");

        $headimgurl = Yii::$app->request->post("headimgurl");


        $comment_content = Yii::$app->request->post("comment");




        date_default_timezone_set('PRC');

        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }





        $comment = new Comment();

        $comment->ip = $ip;
        $comment->browser = $_SERVER['HTTP_USER_AGENT'];
        $comment->status = 0;
        $comment->create_time = date("Y-m-d H:i:s");

        $comment->openid = $openid;
        $comment->nickname = $nickname;
        $comment->headimgurl = $headimgurl;
        $comment->comment = $comment_content;


        $comment->save();


        $data = array("success" => true, "message" => "ok");

        return $data; 

    }


    public function actionGetComments()
    {

        header('Access-Control-Allow-Origin: *');

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;


        $comments = Comment::find()->andWhere(['status' => '1'])->orderBy(new Expression('rand()'))->limit(10)->all();


        $data = array("success" => true, "comments" => $comments);

        return $data; 
    }
    
      public function actionGetCount()
    {  
        $data ="8888888888888888888";
        return $data; 
    }


      public function actionOut()
    { 
        
        $data = "0000000000";
        return $data; 
    }

    public function actionFace()
        { 
              $request = Yii::$app->request; 
            $host = "http://rltz.market.alicloudapi.com/face/detect";
            $path = "/face/detect";
            $method =$request->post('method');
            $picUrl =$request->post('picUrl');
            $appcode = "bea2ed6dcacf4595b827aa4301745bc9"; 
            $headers = array(); 
            array_push($headers, "Authorization:APPCODE " . $appcode);
            
            // //根据API的要求，定义相对应的Content-Type
            array_push($headers, "Content-Type".":"."application/json; charset=UTF-8");
            $querys = "";
            // $bodys = "{\"type\":0,\"image_url\":\"http://rgtest.leow.net.cn/manage/image/test4.jpg\",\"content\":\"base64编码\"}";
            $bodys = "{\"type\":1,\"image_url\":\"\",\"content\":\"$picUrl\"}";
           
            $url = $host;

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($curl, CURLOPT_FAILONERROR, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HEADER, true);
            if (1 == strpos("$".$host, "https://"))
            {
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            }
            curl_setopt($curl, CURLOPT_POSTFIELDS, $bodys);
 
            $domain = strstr(curl_exec($curl), '{');
            // $domain = json_encode($domain, JSON_FORCE_OBJECT);
            // echo $domain; 
            // $data=array("data" => $domain); 
            return $domain;
        } 

    public function actionPicture()
        { 
            
            // $data = '999999999999999999';
            // return $data; 
             $request = Yii::$app->request;
            $sourcePic=$request->post('sourcePic');
            $maskPic=$request->post('maskPic'); 
            $cropPicName=$request->post('cropPicName'); 
            // $maskPic="http://campaign.archisense.cn/bi_test2/assets/images/yinpian3/page2Bg.png";
            // $sourcePic="http://campaign.archisense.cn/bi_test2/assets/images/yinpian3/page2Bg_b.png"; 
            // $cropPicName="sdsd11111";
            $source = imagecreatefrompng( $sourcePic );
            $mask = imagecreatefrompng( $maskPic); 
            $this->imagealphamask ($source, $mask );
            // Output
            header( "Content-type: image/png"); 
   
            imagepng( $source,"assets/images/crop/".$cropPicName.".png" );
            //销毁图片内存
            imagedestroy($source);  
        } 
    public function imagealphamask( &$picture, $mask ) {
            // Get sizes and set up new picture
            $xSize = imagesx( $picture );
            $ySize = imagesy( $picture );
            $newPicture = imagecreatetruecolor( $xSize, $ySize );
            imagesavealpha( $newPicture, true );
            imagefill( $newPicture, 0, 0, imagecolorallocatealpha( $newPicture, 100, 100, 0, 127 ) );
    
            // Perform pixel-based alpha map application
            for( $x = 0; $x < $xSize; $x++ ) {
                for( $y = 0; $y < $ySize; $y++ ) {
                    $alpha = imagecolorsforindex( $mask, imagecolorat( $mask, $x, $y ) );
                    //small mod to extract alpha, if using a black(transparent) and white
                    //mask file instead change the following line back to Jules's original:
                    // $alpha = 127 - floor($alpha['black'] / 2);
                    //or a white(transparent) and black mask file:
                    // $alpha = floor($alpha['black'] / 2);
                    $alpha = $alpha['alpha'];
                    $color = imagecolorsforindex( $picture, imagecolorat( $picture, $x, $y ) );
                    //preserve alpha by comparing the two values
                    if ($color['alpha'] > $alpha)
                        $alpha = $color['alpha'];
                    //kill data for fully transparent pixels
                    if ($alpha == 127) {
                        $color['red'] = 0;
                        $color['blue'] = 0;
                        $color['green'] = 0;
                    }
                    imagesetpixel( $newPicture, $x, $y, imagecolorallocatealpha( $newPicture, $color[ 'red' ], $color[ 'green' ], $color[ 'blue' ], $alpha ) );
                }
            }

            // Copy back to original picture
            imagedestroy( $picture );
            $picture = $newPicture;
        } 

   
























}
